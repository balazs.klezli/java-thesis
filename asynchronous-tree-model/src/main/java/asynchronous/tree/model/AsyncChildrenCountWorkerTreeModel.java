package asynchronous.tree.model;

import asynchronous.tree.data.provider.ChildrenCountDataProvider;
import asynchronous.tree.node.DummyNode;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static asynchronous.tree.util.TreeNodeUtil.getPath;

/**
 * Asynchronous implementation of {@link AsyncChildrenWorkerTreeModel}. It can be used as an asynchronous model for a {@link JTree}. The constructor method requires an
 * implementation of the {@link ChildrenCountDataProvider} as a parameter.
 */
public class AsyncChildrenCountWorkerTreeModel extends AsyncChildrenWorkerTreeModel {

  private final Map<TreeNode, ChildrenCountSwingWorker> childrenCountSwingWorkers = new HashMap<>();

  private ChildrenCountDataProvider<TreeNode> childrenCountDataProvider = null;

  public AsyncChildrenCountWorkerTreeModel(ChildrenCountDataProvider<? extends TreeNode>  childrenCountDataProvider) {
    super(childrenCountDataProvider);
    this.childrenCountDataProvider = (ChildrenCountDataProvider<TreeNode>) childrenCountDataProvider;
  }

  @Override
  public int getChildCount(Object parent) {
    if (!(parent instanceof TreeNode)) {
      throw new IllegalStateException("parent is not a tree node");
    }
    TreeNode parentNode = (TreeNode) parent;
    if (parentNode instanceof DummyNode) {
      return 0;
    }

    ChildrenCountSwingWorker childrenCountSwingWorker = childrenCountSwingWorkers.get(parentNode);

    if (childrenCountSwingWorker == null) {
      childrenCountSwingWorker = createNewChildrenCountSwingWorker(parentNode);
    }

    return childrenCountSwingWorker.getChildrenCount();
  }

  private ChildrenCountSwingWorker createNewChildrenCountSwingWorker(TreeNode node) {
    ChildrenCountSwingWorker childrenCountSwingWorker = new ChildrenCountSwingWorker((TreeNode) node);
    childrenCountSwingWorkers.put(node, childrenCountSwingWorker);
    childrenCountSwingWorker.execute();
    return childrenCountSwingWorker;
  }

  @Override
  protected void invalidateWorkers() {
    for (TreeNode node : childrenCountSwingWorkers.keySet()) {
      ChildrenCountSwingWorker newChildrenCountSwingWorker = new ChildrenCountSwingWorker((TreeNode) node);
      childrenCountSwingWorkers.put(node, newChildrenCountSwingWorker);
      newChildrenCountSwingWorker.execute();
    }
  }

  /**
   * {@link SwingWorker} implementation which retrieves the count of the children of the given parent node. It uses the {@link ChildrenCountDataProvider}.
   */
  class ChildrenCountSwingWorker extends SwingWorker<Integer, Void> {

    private Integer childrenCount = 0;

    public Integer getChildrenCount() {
      return childrenCount;
    }

    private final TreeNode node;

    ChildrenCountSwingWorker(TreeNode node) {
      this.node = node;
    }

    @Override
    protected Integer doInBackground() throws Exception {
      return childrenCountDataProvider.getChildrenCount(node);
    }

    @Override
    protected void done() {
      try {
        childrenCount = get();
        TreeNode parent = node.getParent();
        if (parent == null) {
          TreeNode[] rootPath = {node};
          fireTreeStructureChanged(this, rootPath, null, null);
        } else {
          TreeNode[] treePath = getPath(parent);
          int[] parentIndex = {parent.getIndex(node)};
          TreeNode[] changedElement = {node};
          fireTreeStructureChanged(this, treePath, parentIndex, changedElement);
        }
      } catch (InterruptedException | ExecutionException e) {
        fireExceptionEvent(e);
      }
    }
  }
}
