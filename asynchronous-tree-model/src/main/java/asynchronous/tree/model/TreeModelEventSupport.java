package asynchronous.tree.model;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

/**
 * Supporter class for the tree model events.
 */
public class TreeModelEventSupport {

  private final List<TreeModelListener> listeners = new ArrayList<>();

  /**
   * Function to add a tree model listener which will be informed when a tree model event happens
   *
   * @param l the listener which will be added to the tree model listeners
   */
  protected void addListener(TreeModelListener l) {
    if (l != null) {
      listeners.add(l);
    }
  }

  /**
   * Function to remove a tree model listener
   *
   * @param l the listener which will be removed from the tree model listeners
   */
  protected void removeListener(TreeModelListener l) {
    if (l != null) {
      listeners.remove(l);
    }
  }

  /**
   * Notifies all listeners that have registered interest for notification on this event type. The event instance is lazily created using the parameters passed into the fire
   * method.
   *
   * @param source the source of the {@code TreeModelEvent}; typically {@code this}
   * @param path the path to the parent of the nodes that changed; use {@code null} to identify the root has changed
   * @param childIndices the indices of the changed elements
   * @param children the changed elements
   */
  protected void fireTreeNodesChanged(Object source, Object[] path, int[] childIndices, Object[] children) {
    TreeModelEvent e = new TreeModelEvent(source, path, childIndices, children);
    for (TreeModelListener tml : listeners) {
      tml.treeNodesChanged(e);
    }
  }

  /**
   * Notifies all listeners that have registered interest for notification on this event type. The event instance is lazily created using the parameters passed into the fire
   * method.
   *
   * @param source the source of the {@code TreeModelEvent}; typically {@code this}
   * @param path the path to the parent the nodes were added to
   * @param childIndices the indices of the new elements
   * @param children the new elements
   */
  protected void fireTreeNodesInserted(Object source, Object[] path, int[] childIndices, Object[] children) {
    TreeModelEvent e = new TreeModelEvent(source, path, childIndices, children);
    for (TreeModelListener tml : listeners) {
      tml.treeNodesInserted(e);
    }
  }

  /**
   * Notifies all listeners that have registered interest for notification on this event type. The event instance is lazily created using the parameters passed into the fire
   * method.
   *
   * @param source the source of the {@code TreeModelEvent}; typically {@code this}
   * @param path the path to the parent the nodes were removed from
   * @param childIndices the indices of the removed elements
   * @param children the removed elements
   */
  protected void fireTreeNodesRemoved(Object source, Object[] path, int[] childIndices, Object[] children) {
    TreeModelEvent e = new TreeModelEvent(source, path, childIndices, children);
    for (TreeModelListener tml : listeners) {
      tml.treeNodesRemoved(e);
    }
  }

  /**
   * Notifies all listeners that have registered interest for notification on this event type. The event instance is lazily created using the parameters passed into the fire
   * method.
   *
   * @param source the source of the {@code TreeModelEvent}; typically {@code this}
   * @param path the path to the parent of the structure that has changed; use {@code null} to identify the root has changed
   * @param childIndices the indices of the affected elements
   * @param children the affected elements
   */
  protected void fireTreeStructureChanged(Object source, Object[] path, int[] childIndices, Object[] children) {
    TreeModelEvent e = new TreeModelEvent(source, path, childIndices, children);
    for (TreeModelListener tml : listeners) {
      tml.treeStructureChanged(e);
    }
  }

  /**
   * Notifies all listeners that have registered interest for notification on this event type. The event instance is lazily created using the parameters passed into the fire
   * method.
   *
   * @param source the source of the {@code TreeModelEvent}; typically {@code this}
   * @param path the path to the parent of the structure that has changed; use {@code null} to identify the root has changed
   */
  protected void fireTreeStructureChanged(Object source, TreePath path) {
    TreeModelEvent e = new TreeModelEvent(source, path);
    for (TreeModelListener tml : listeners) {
      tml.treeStructureChanged(e);
    }
  }
}
