package asynchronous.tree.model;

import asynchronous.tree.data.provider.ChildrenDataProvider;
import asynchronous.tree.node.DummyNode;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static asynchronous.tree.util.TreeNodeUtil.getPath;
import static javax.swing.SwingWorker.StateValue.PENDING;

/**
 * Asynchronous implementation of {@link AsyncChildrenWorkerTreeModel} where every children has its own worker.
 * It can be used as an asynchronous model for a {@link JTree}.
 * The constructor method requires an implementation of the {@link ChildrenDataProvider} as a parameter.
 */
public class AsyncChildrenWorkerTreeModel extends AbstractAsyncTreeModel {

  private RootSwingWorker rootSwingWorker = new RootSwingWorker();

  protected Map<TreeNode, ChildSwingWorker> childSwingWorkers = new HashMap<>();

  private final ChildrenDataProvider<TreeNode> dataProvider;

  public AsyncChildrenWorkerTreeModel(ChildrenDataProvider<? extends TreeNode> dataProvider) {
    if (dataProvider == null) {
      throw new IllegalStateException("Data provider should be not null");
    }
    this.dataProvider = (ChildrenDataProvider<TreeNode>) dataProvider;
  }

  protected ChildrenDataProvider<TreeNode> getDataProvider() {
    if (dataProvider == null) {
      throw new IllegalStateException("Data provider is not set");
    }
    return dataProvider;
  }

  @Override
  public Object getRoot() {
    if (rootSwingWorker.isInitialState()) {
      rootSwingWorker.execute();
    }

    return rootSwingWorker.getRoot();
  }

  @Override
  public int getChildCount(Object parent) {
    if (!(parent instanceof TreeNode)) {
      throw new IllegalStateException("parent is not a tree node");
    }
    TreeNode parentNode = (TreeNode) parent;

    if (parentNode instanceof DummyNode) {
      return 0;
    }

    ChildSwingWorker childSwingWorker = childSwingWorkers.get(parentNode);
    if (childSwingWorker == null) {
      childSwingWorker = new ChildSwingWorker((TreeNode) parentNode);
      childSwingWorkers.put(parentNode, childSwingWorker);
      childSwingWorker.execute();
    }

    return childSwingWorker.getChildren().size();
  }

  @Override
  public Object getChild(Object parent, int index) {
    if (!(parent instanceof TreeNode)) {
      throw new IllegalStateException("parent is not a treenode");
    }
    TreeNode parentNode = (TreeNode) parent;
    ChildSwingWorker childSwingWorker = childSwingWorkers.get(parentNode);

    if (childSwingWorker == null) {
      childSwingWorker = new ChildSwingWorker((TreeNode) parentNode);
      childSwingWorkers.put(parentNode, childSwingWorker);
      childSwingWorker.execute();
    }

    final List<TreeNode> children = childSwingWorker.getChildren();
    if (children.isEmpty()) {
      return new DummyNode();
    } else {
      return children.get(index);
    }
  }

  @Override
  public int getIndexOfChild(Object parent, Object child) {
    if (!(parent instanceof TreeNode)) {
      throw new IllegalStateException("parent is not a treenode");
    }
    TreeNode parentNode = (TreeNode) parent;
    ChildSwingWorker childSwingWorker = childSwingWorkers.get(parentNode);

    if (childSwingWorker == null) {
      childSwingWorker = new ChildSwingWorker((TreeNode) parentNode);
      childSwingWorkers.put(parentNode, childSwingWorker);
      childSwingWorker.execute();
    }

    return childSwingWorker.getChildren().indexOf(child);
  }

  @Override
  protected void refreshRootSwingWorker() {
    rootSwingWorker = new RootSwingWorker();
    rootSwingWorker.execute();
  }

  @Override
  protected void refreshChildSwingWorkers() {
    for (TreeNode node : childSwingWorkers.keySet()) {
      ChildSwingWorker newChildSwingWorker = new ChildSwingWorker((TreeNode) node);
      childSwingWorkers.put(node, newChildSwingWorker);
      newChildSwingWorker.execute();
    }
  }

  @Override
  protected void invalidateWorkers() {
  }

  /**
   * {@link SwingWorker} implementation which retrieves the root node of the tree. It uses the {@link ChildrenDataProvider}.
   */
  class RootSwingWorker extends SwingWorker<TreeNode, Void> {

    private TreeNode root = createDummyNode();

    public TreeNode getRoot() {
      return root;
    }

    public boolean isInitialState() {
      return this.getState() == PENDING;
    }

    @Override
    protected TreeNode doInBackground() throws Exception {
      return getDataProvider().getRoot();
    }

    @Override
    protected void done() {
      try {
        root = get();
        fireTreeStructureChanged(this, null);
      } catch (ExecutionException | InterruptedException e) {
        fireExceptionEvent(e);
      }
    }
  }

  /**
   * {@link SwingWorker} implementation which retrieves the list of the children nodes of a given parent node. It uses the {@link ChildrenDataProvider}.
   */
  protected class ChildSwingWorker extends SwingWorker<List<TreeNode>, Void> {

    private List<TreeNode> children = new ArrayList<>();

    private final TreeNode node;

    ChildSwingWorker(TreeNode node) {
      this.node = node;
    }

    public List<TreeNode> getChildren() {
      return children;
    }

    @Override
    protected List<TreeNode> doInBackground() throws Exception {
      return getDataProvider().getChildren(node);
    }

    @Override
    protected void done() {
      try {
        children = get();
        TreeNode[] treePath = getPath(node);
        fireTreeStructureChanged(this, treePath, getIndices(children.size()), children.toArray());
      } catch (ExecutionException | InterruptedException e) {
        fireExceptionEvent(e);
      }
    }
  }
}
