package asynchronous.tree.model;

import asynchronous.exception.ExceptionHandler;
import asynchronous.exception.ExceptionHandlerImpl;
import asynchronous.exception.event.ExceptionListener;
import asynchronous.tree.node.DummyNode;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.*;
import java.util.stream.IntStream;

/**
 * Abstract asynchronous implementation for {@link TreeModel} which extends {@link TreeModelEventSupport}.
 */
public abstract class AbstractAsyncTreeModel extends TreeModelEventSupport implements TreeModel {

  private final ExceptionHandler eh = new ExceptionHandlerImpl();

  /**
   * Function to add an exception listener which will be informed when some error occurres in the data source.
   *
   * @param listener the listener which will be added to the exception listeners
   */
  public void addExceptionListener(ExceptionListener listener) {
    eh.addExceptionListener(listener);
  }

  /**
   * Function to remove an exception listener.
   *
   * @param listener the listener which will be removed from the exception listeners
   */
  public void removeExceptionListener(ExceptionListener listener) {
    eh.removeExceptionListener(listener);
  }

  /**
   * Notifies the listeners about the given exception.
   *
   * @param exception the exception which will be sent to the listeners
   */
  protected void fireExceptionEvent(Exception exception) {
    eh.fireExceptionEvent(exception);
  }

  @Override
  public void addTreeModelListener(TreeModelListener l) {
    addListener(l);
  }

  @Override
  public void removeTreeModelListener(TreeModelListener l) {
    removeListener(l);
  }

  @Override
  public void valueForPathChanged(TreePath path, Object newValue) {
  }

  /**
   * Returns with an integer array which contains the number sequence starting with zero and ending with the given number.
   *
   * @param number the biggest number which will be in the array
   * @return the integer array
   */
  protected int[] getIndices(int number) {
    return IntStream.range(0, number).toArray();
  }

  /**
   * Creates a new dummy node under the given parent node.
   *
   * @param parentNode the parent of the new dummy node
   * @return the newly created dummy node
   */
  protected DefaultMutableTreeNode createDummyNode(TreeNode parentNode) {
    final DefaultMutableTreeNode dummyTreeNode = new DummyNode();
    if (parentNode != null) {
      if (!(parentNode instanceof MutableTreeNode)) {
        throw new IllegalStateException("parent node is not mutable");
      }
      ((MutableTreeNode) parentNode).insert(dummyTreeNode, parentNode.getChildCount());
    }
    return dummyTreeNode;
  }

  /**
   * Creates a dummy root node.
   *
   * @return the newly created dummy root node
   */
  protected DefaultMutableTreeNode createDummyNode() {
    return createDummyNode(null);
  }

  /**
   * Function for refreshing the root SwingWorker.
   */
  protected abstract void refreshRootSwingWorker();

  /**
   * Function for refreshing the child SwingWorkers.
   */
  protected abstract void refreshChildSwingWorkers();

  /**
   * Function for invalidate all SwingWorkers.
   */
  protected abstract void invalidateWorkers();

  /**
   * Invalidates all data retrieved from the provider. This will cause the {@link JTree}s to be refreshed.
   */
  public void invalidateData() {
    refreshRootSwingWorker();
    refreshChildSwingWorkers();
    invalidateWorkers();
    fireTreeStructureChanged(this, null);
  }

  @Override
  public boolean isLeaf(Object node) {
    return getChildCount(node) == 0;
  }
}
