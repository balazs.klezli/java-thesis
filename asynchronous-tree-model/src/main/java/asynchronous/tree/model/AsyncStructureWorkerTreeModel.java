package asynchronous.tree.model;

import asynchronous.tree.data.provider.StructureDataProvider;
import asynchronous.tree.node.DummyNode;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import java.util.concurrent.ExecutionException;

import static javax.swing.SwingWorker.StateValue.PENDING;

/**
 * Asynchronous implementation of {@link AbstractAsyncTreeModel}. It can be used as an asynchronous model for a {@link JTree}.
 * The constructor method requires an implementation of the {@link StructureDataProvider} as a parameter.
 */
public class AsyncStructureWorkerTreeModel extends AbstractAsyncTreeModel {

  private StructureDataProvider<TreeNode> dataProvider = null;

  private DataSwingWorker dataSwingWorker = new DataSwingWorker();

  public AsyncStructureWorkerTreeModel(StructureDataProvider<? extends TreeNode> dataProvider) {
    if (dataProvider == null) {
      throw new IllegalStateException("Data provider should be not null");
    }
    this.dataProvider = (StructureDataProvider<TreeNode>) dataProvider;
  }

  @Override
  public Object getRoot() {
    if (dataSwingWorker.isInitialState()) {
      dataSwingWorker.execute();
    }

    return dataSwingWorker.getRoot();
  }

  @Override
  public Object getChild(Object parent, int index) {
    if (!(parent instanceof TreeNode)) {
      throw new IllegalStateException("parent is not a tree node");
    }
    TreeNode parentNode = (TreeNode) parent;
    return parentNode.getChildAt(index);
  }

  @Override
  public int getChildCount(Object parent) {
    if (!(parent instanceof TreeNode)) {
      throw new IllegalStateException("parent is not a tree node");
    }
    TreeNode parentNode = (TreeNode) parent;

    if (parentNode instanceof DummyNode) {
      return 0;
    }

    return parentNode.getChildCount();
  }

  @Override
  public int getIndexOfChild(Object parent, Object child) {
    if (!(parent instanceof TreeNode)) {
      throw new IllegalStateException("parent is not a treenode");
    }
    TreeNode parentNode = (TreeNode) parent;
    if (!(child instanceof TreeNode)) {
      throw new IllegalStateException("parent is not a treenode");
    }
    TreeNode childNode = (TreeNode) child;
    return parentNode.getIndex(childNode);
  }

  @Override
  protected void refreshRootSwingWorker() {
    dataSwingWorker = new DataSwingWorker();
    dataSwingWorker.execute();
  }

  @Override
  protected void refreshChildSwingWorkers() {
  }

  @Override
  protected void invalidateWorkers() {
  }

  /**
   * {@link SwingWorker} implementation which retrieves thr root node with the full tree structure. It uses the {@link StructureDataProvider}.
   */
  class DataSwingWorker extends SwingWorker<TreeNode, Void> {

    private TreeNode root = createDummyNode();

    public TreeNode getRoot() {
      return root;
    }

    public boolean isInitialState() {
      return this.getState() == PENDING;
    }

    @Override
    protected TreeNode doInBackground() throws Exception {
      return dataProvider.getData();
    }

    @Override
    protected void done() {
      try {
        root = get();
        fireTreeStructureChanged(this, null);
      } catch (InterruptedException | ExecutionException e) {
        fireExceptionEvent(e);
      }
    }
  }
}
