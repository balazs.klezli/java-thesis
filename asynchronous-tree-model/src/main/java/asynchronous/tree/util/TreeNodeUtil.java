package asynchronous.tree.util;

import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Utility class for tree operations. It contains helper functions for {@link TreeNode} based nodes.
 */
public class TreeNodeUtil {

  /**
   * Returns with an array which contains the path of the given node.
   *
   * @param node the node of a tree
   * @return the path of the node as an array
   */
  public static TreeNode[] getPath(TreeNode node) {
    checkNullNode(node);

    List<TreeNode> path = new ArrayList<>();
    path.add(node);
    TreeNode parent = node.getParent();
    while (parent != null) {
      path.add(parent);
      parent = parent.getParent();
    }
    Collections.reverse(path);
    return path.toArray(new TreeNode[path.size()]);
  }

  private static void checkNullNode(TreeNode node) {
    if (node == null) {
      throw new IllegalArgumentException("Node cannot be null.");
    }
  }
}
