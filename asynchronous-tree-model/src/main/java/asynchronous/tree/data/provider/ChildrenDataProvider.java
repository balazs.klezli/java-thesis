package asynchronous.tree.data.provider;

import javax.swing.tree.TreeNode;
import java.util.List;

/**
 * Data provider interface which has a function for getting the root tree node and a function for getting the list of the children of a given parent node.
 *
 * @param <E> type parameter for tree nodes - extends {@link TreeNode}
 */
public interface ChildrenDataProvider<E extends TreeNode> {

  Object DUMMY_DATA = "?";

  /**
   * Retrieves the root tree node.
   *
   * @return the root node
   * @throws Exception when some error occurres during the root node retrieval
   */
  E getRoot() throws Exception;

  /**
   * Retrieves the list of the children based on the given parent
   *
   * @param parent the node of which the children are needed to be retrieved
   * @return the list of the children of the parent node
   * @throws Exception when some error occurres during children retrieval
   */
  List<E> getChildren(E parent) throws Exception;
}
