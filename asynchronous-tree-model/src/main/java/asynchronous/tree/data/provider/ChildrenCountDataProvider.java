package asynchronous.tree.data.provider;

import javax.swing.tree.TreeNode;

/**
 * Data provider interface which extends {@link ChildrenDataProvider} and has a function for getting the count of the children of a given parent node.
 *
 * @param <E> type parameter of the tree nodes - extends {@link TreeNode}
 */
public interface ChildrenCountDataProvider<E extends TreeNode> extends ChildrenDataProvider<E> {

  /**
   * Retrieves the count of the children based on the given parent node
   *
   * @param parent the parent tree node
   * @return the count of children for the given parent node
   * @throws Exception when some error occurres during the retrieval of the count of the children
   */
  Integer getChildrenCount(E parent) throws Exception;
}
