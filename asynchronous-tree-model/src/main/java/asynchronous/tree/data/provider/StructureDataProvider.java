package asynchronous.tree.data.provider;

import javax.swing.tree.TreeNode;

/**
 * Data provider interface which has a function for getting the tree root node. This root node should contain the full tree structure.
 *
 * @param <E> type parameter of the tree nodes - extends {@link TreeNode}
 */
public interface StructureDataProvider<E extends TreeNode> {

  /**
   * Retrieves the root node which contains the full tree structure.
   *
   * @return the root node with the full tree structure
   * @throws Exception when some error occurres during data retrieval
   */
  E getData() throws Exception;
}
