package asynchronous.tree.node;

import javax.swing.tree.DefaultMutableTreeNode;

import static asynchronous.tree.data.provider.ChildrenDataProvider.DUMMY_DATA;

/**
 * Type to define a dummy node - such a node is unknown yet, loading of it is still in progress in the background.
 */
public class DummyNode extends DefaultMutableTreeNode {

  public DummyNode() {
    super(DUMMY_DATA, false);
  }
}
