package asynchronous.tree.model;

import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;
import asynchronous.tree.data.provider.ChildrenDataProvider;
import asynchronous.tree.node.DummyNode;
import java.util.ArrayList;
import java.util.List;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import static java.util.concurrent.TimeUnit.SECONDS;
import javax.swing.event.TreeModelListener;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.after;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Test class for testing common parts of the tree model.
 */
public class AsyncChildrenWorkerTreeModelTest {

  private static final String ANY_NODE_NAME = "anyNodeName";

  private static final String ANY_PROVIDER_EXCEPTION_MESSAGE = "anyProviderErrorHappened";

  private static final Object SYNC_OBJECT = new Object();

  private static final String ANY_ROOT_NODE = "anyRootNode";

  private ArgumentCaptor<ExceptionEvent> exceptionEventArgumentCaptor;

  @Mock
  private ChildrenDataProvider<TreeNode> dataProviderMock;

  @Mock
  private ExceptionListener exceptionListenerMock;

  @Mock
  private TreeModelListener treeModelListenerMock;

  AsyncChildrenWorkerTreeModel underTest;

  @BeforeMethod
  public void init() {
    MockitoAnnotations.initMocks(this);
    exceptionEventArgumentCaptor = ArgumentCaptor.forClass(ExceptionEvent.class);

    underTest = new AsyncChildrenWorkerTreeModel(dataProviderMock);
    underTest.addExceptionListener(exceptionListenerMock);
    underTest.addTreeModelListener(treeModelListenerMock);
  }

  @Test
  public void shouldReturnsDummyNodeWithoutWaiting() throws Exception {
    Object result = underTest.getRoot();

    assertTrue(result instanceof DummyNode);
  }

  @Test
  public void shouldReturnsWithRootNodeWhenWaitForIt() throws Exception {
    when(dataProviderMock.getRoot()).thenReturn(new DefaultMutableTreeNode(ANY_ROOT_NODE));

    await().atMost(5, SECONDS).until(() -> !(underTest.getRoot() instanceof DummyNode));

    TreeNode result = (TreeNode) underTest.getRoot();
    assertEquals(result.toString(), ANY_ROOT_NODE);
  }

  @Test
  public void shouldFireExceptionEventIfSomethingWentWrongInTheBackgroundThread() throws Throwable {
    // Given
    when(dataProviderMock.getRoot()).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.getRoot();

    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(500);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getCause().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }

  @Test
  public void shouldReturnZeroWhenGetChildCountCalledWithDummyNode() throws Exception {
    TreeNode dummyNode = new DefaultMutableTreeNode();

    int actualChildCount = underTest.getChildCount(dummyNode);

    assertEquals(actualChildCount, 0, "Dummy node doesn't have children.");
  }

  @Test
  public void shouldReturnZeroWhenGetChildCountCalledWithLeafNode() throws Exception {
    // Given
    TreeNode leafNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    when(dataProviderMock.getChildren(leafNode)).thenReturn(new ArrayList<>());

    // When
    underTest.getChildCount(leafNode);
    verify(treeModelListenerMock, after(200).atLeastOnce()).treeStructureChanged(any());
    int actualChildCount = underTest.getChildCount(leafNode);

    // Then
    assertEquals(actualChildCount, 0, "Leaf node doesn't have children");
  }

  @Test
  public void shouldReturnNumberOfChildWhenGetChildCountCalledWithParentNode() throws Exception {
    // Given
    List<TreeNode> listOfChildren = new ArrayList<>();
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);

    listOfChildren.add(new DefaultMutableTreeNode("child1"));
    listOfChildren.add(new DefaultMutableTreeNode("child2"));
    listOfChildren.add(new DefaultMutableTreeNode("child3"));
    for (TreeNode treeNode : listOfChildren) {
      parentNode.add((DefaultMutableTreeNode) treeNode);
    }

    when(dataProviderMock.getChildren(parentNode)).thenReturn(listOfChildren);

    // When
    underTest.getChildCount(parentNode);
    verify(treeModelListenerMock, after(200).atLeastOnce()).treeStructureChanged(any());
    int actualChildCount = underTest.getChildCount(parentNode);

    // Then
    assertEquals(actualChildCount, 3, "Should returns with the number of the children");
  }

  @Test
  public void shouldFireExceptionEventWhenGetChildCountCalledAndSomethingWentWrongInTheBackendThread() throws Exception {
    // Given
    DefaultMutableTreeNode leafNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    when(dataProviderMock.getChildren(leafNode)).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.getChildCount(leafNode);

    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(500);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getCause().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }

  @Test
  public void shouldReturnTrueWhenIsLeafCalledWithDummyNode() throws Exception {
    DummyNode dummyNode = new DummyNode();

    boolean isLeaf = underTest.isLeaf(dummyNode);

    assertEquals(isLeaf, true, "Dummy nodes are always leaf nodes");
  }

  @Test
  public void shouldReturnTrueWhenIsLeafCalledWithLeafNode() throws Exception {
    // Given
    DefaultMutableTreeNode leafNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    when(dataProviderMock.getChildren(leafNode)).thenReturn(new ArrayList<>());

    // When
    underTest.isLeaf(leafNode);
    verify(treeModelListenerMock, after(200).atLeastOnce()).treeStructureChanged(any());
    boolean isLeaf = underTest.isLeaf(leafNode);

    // Then
    assertTrue(isLeaf, "Leaf node doesn't have children");
  }

  @Test
  public void shouldReturnFalseWhenIsLeafCalledWithParentNode() throws Exception {
    // Given
    List<TreeNode> listOfChildren = new ArrayList<>();
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);

    listOfChildren.add(new DefaultMutableTreeNode("child1"));
    listOfChildren.add(new DefaultMutableTreeNode("child2"));
    listOfChildren.add(new DefaultMutableTreeNode("child3"));
    when(dataProviderMock.getChildren(parentNode)).thenReturn(listOfChildren);

    // When
    underTest.isLeaf(parentNode);
    verify(treeModelListenerMock, after(200).atLeastOnce()).treeStructureChanged(any());
    boolean isLeaf = underTest.isLeaf(parentNode);

    // Then
    assertFalse(isLeaf, "Parent node has children so it is not a leaf");
  }

  @Test
  public void shouldFireExceptionEventWhenIsLeafCalledAndSomethingWentWrongInTheBackendThread() throws Exception {
    // Given
    DefaultMutableTreeNode leafNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    when(dataProviderMock.getChildren(leafNode)).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.isLeaf(leafNode);

    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(500);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getCause().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }

  @Test
  public void shouldReturnChildWhenGetChildNodeCalledWithParentNodeAndRightIndex() throws Exception {
    // Given
    List<TreeNode> listOfChildren = new ArrayList<>();
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    String nameOfTheFirstChild = "child1";
    String nameOfTheSecondChild = "child2";
    String nameOfTheThirdChild = "child3";

    DefaultMutableTreeNode firstChild = new DefaultMutableTreeNode(nameOfTheFirstChild);
    DefaultMutableTreeNode secondChild = new DefaultMutableTreeNode(nameOfTheSecondChild);
    DefaultMutableTreeNode thirdChild = new DefaultMutableTreeNode(nameOfTheThirdChild);

    parentNode.add(firstChild);
    parentNode.add(secondChild);
    parentNode.add(thirdChild);

    listOfChildren.add(firstChild);
    listOfChildren.add(secondChild);
    listOfChildren.add(thirdChild);
    when(dataProviderMock.getChildren(parentNode)).thenReturn(listOfChildren);

    // When
    underTest.getChild(parentNode, 0);
    verify(treeModelListenerMock, after(200).atLeastOnce()).treeStructureChanged(any());
    firstChild = (DefaultMutableTreeNode) underTest.getChild(parentNode, 0);
    secondChild = (DefaultMutableTreeNode) underTest.getChild(parentNode, 1);
    thirdChild = (DefaultMutableTreeNode) underTest.getChild(parentNode, 2);

    // Then
    assertEquals(firstChild.toString(), nameOfTheFirstChild);
    assertEquals(secondChild.toString(), nameOfTheSecondChild);
    assertEquals(thirdChild.toString(), nameOfTheThirdChild);
  }

  @Test
  public void shouldReturnIndexOfTheGivenChildNode() throws Exception {
    // Given
    List<TreeNode> listOfChildren = new ArrayList<>();
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    DefaultMutableTreeNode firstChild = new DefaultMutableTreeNode("child1");
    DefaultMutableTreeNode secondChild = new DefaultMutableTreeNode("child2");
    DefaultMutableTreeNode thirdChild = new DefaultMutableTreeNode("child3");

    parentNode.add(firstChild);
    parentNode.add(secondChild);
    parentNode.add(thirdChild);

    listOfChildren.add(firstChild);
    listOfChildren.add(secondChild);
    listOfChildren.add(thirdChild);
    when(dataProviderMock.getChildren(parentNode)).thenReturn(listOfChildren);

    // When
    underTest.getIndexOfChild(parentNode, firstChild);
    verify(treeModelListenerMock, after(200).atLeastOnce()).treeStructureChanged(any());
    int indexOfTheFirstChild = underTest.getIndexOfChild(parentNode, firstChild);
    int indexOfTheSecondChild = underTest.getIndexOfChild(parentNode, secondChild);
    int indexOfTheThirdChild = underTest.getIndexOfChild(parentNode, thirdChild);

    // Then
    assertEquals(indexOfTheFirstChild, 0);
    assertEquals(indexOfTheSecondChild, 1);
    assertEquals(indexOfTheThirdChild, 2);
  }

  @Test
  public void shouldReturnMinusOneWhenTheGivenChildNodeIsNotTheParentNodeChild() throws Exception {
    // Given
    List<TreeNode> listOfChildren = new ArrayList<>();
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    DefaultMutableTreeNode firstChild = new DefaultMutableTreeNode("child1");
    DefaultMutableTreeNode secondChild = new DefaultMutableTreeNode("child2");
    DefaultMutableTreeNode thirdChild = new DefaultMutableTreeNode("child3");

    parentNode.add(firstChild);
    parentNode.add(secondChild);
    parentNode.add(thirdChild);

    listOfChildren.add(firstChild);
    listOfChildren.add(secondChild);
    listOfChildren.add(thirdChild);

    DefaultMutableTreeNode childButNotForTheParentNode = new DefaultMutableTreeNode("child of the first childNode");

    listOfChildren.add(firstChild);
    listOfChildren.add(secondChild);
    listOfChildren.add(thirdChild);
    when(dataProviderMock.getChildren(parentNode)).thenReturn(listOfChildren);

    // When
    underTest.getIndexOfChild(parentNode, childButNotForTheParentNode);
    verify(treeModelListenerMock, after(200).atLeastOnce()).treeStructureChanged(any());
    int indexOfTheChildButNotForTheParentNode = underTest.getIndexOfChild(parentNode, childButNotForTheParentNode);

    // Then
    assertEquals(indexOfTheChildButNotForTheParentNode, -1);
  }
}
