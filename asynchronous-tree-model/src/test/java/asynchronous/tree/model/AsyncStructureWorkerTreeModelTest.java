package asynchronous.tree.model;

import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;
import asynchronous.tree.data.provider.StructureDataProvider;
import asynchronous.tree.node.DummyNode;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class AsyncStructureWorkerTreeModelTest {

  private static final String ANY_NODE_NAME = "anyNodeName";

  private static final String ANY_PROVIDER_EXCEPTION_MESSAGE = "anyProviderErrorHappened";

  private static final Object SYNC_OBJECT = new Object();

  private ArgumentCaptor<ExceptionEvent> exceptionEventArgumentCaptor;

  @Mock
  private StructureDataProvider<TreeNode> dataProviderMock;

  @Mock
  private ExceptionListener exceptionListenerMock;

  @Mock
  private TreeModelListener treeModelListenerMock;

  AsyncStructureWorkerTreeModel underTest;

  @BeforeMethod
  public void init() {
    MockitoAnnotations.initMocks(this);
    exceptionEventArgumentCaptor = ArgumentCaptor.forClass(ExceptionEvent.class);

    underTest = new AsyncStructureWorkerTreeModel(dataProviderMock);
    underTest.addExceptionListener(exceptionListenerMock);
    underTest.addTreeModelListener(treeModelListenerMock);
  }

  @Test
  public void shouldReturnDummyNodeWhenGetRootNodeCalledWithoutWait() throws Exception {
    Object node = underTest.getRoot();

    assertTrue(node instanceof DummyNode, "Dummy node doesn't have children.");
  }

  @Test
  public void shouldReturnsWithRootNodeWhenWaitForIt() throws Exception {
    when(dataProviderMock.getData()).thenReturn(new DefaultMutableTreeNode(ANY_NODE_NAME));

    await().atMost(5, SECONDS).until(() -> !(underTest.getRoot() instanceof DummyNode));

    Object result = underTest.getRoot();
    assertEquals(result.toString(), ANY_NODE_NAME);
  }

  @Test
  public void shouldFireExceptionEventIfSomethingWentWrongInTheBackgroundThread() throws Throwable {
    // Given
    when(dataProviderMock.getData()).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.getRoot();

    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(500);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getCause().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }

  @Test
  public void shouldReturnChildWhenGetChildCalledWithValidAttributes() throws Exception {
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    DefaultMutableTreeNode child1 = new DefaultMutableTreeNode("child 1");
    DefaultMutableTreeNode child2 = new DefaultMutableTreeNode("child 2");
    parentNode.add(child1);
    parentNode.add(child2);

    Object result = underTest.getChild(parentNode, 1);
    assertEquals(result.toString(), child2.toString());
  }

  @Test
  public void shouldReturnZeroWhenGetChildCountCalledWithLeafNode() throws Exception {
    DefaultMutableTreeNode leafNode = new DefaultMutableTreeNode(ANY_NODE_NAME);

    int numberOfChildren = underTest.getChildCount(leafNode);
    assertEquals(numberOfChildren, 0);
  }

  @Test
  public void shouldReturnNumberOfChildrenWhenGetChildCountCalledWithParentNode() throws Exception {
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    DefaultMutableTreeNode child1 = new DefaultMutableTreeNode("child 1");
    DefaultMutableTreeNode child2 = new DefaultMutableTreeNode("child 2");

    parentNode.add(child1);
    parentNode.add(child2);

    int numberOfChildren = underTest.getChildCount(parentNode);
    assertEquals(numberOfChildren, 2);
  }

  @Test
  public void shouldReturnTrueWhenIsLeafCalledWithLeafNode() throws Exception {
    DefaultMutableTreeNode leafNode = new DefaultMutableTreeNode(ANY_NODE_NAME);

    boolean result = underTest.isLeaf(leafNode);
    assertEquals(result, true);
  }

  @Test
  public void shouldReturnFalseWhenIsLeafCalledWithParentNode() throws Exception {
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    DefaultMutableTreeNode child1 = new DefaultMutableTreeNode("child 1");
    DefaultMutableTreeNode child2 = new DefaultMutableTreeNode("child 2");

    parentNode.add(child1);
    parentNode.add(child2);

    boolean result = underTest.isLeaf(parentNode);
    assertEquals(result, false);
  }

  @Test
  public void shouldReturnWithIndexOfNodeWhenGetIndexOfChildCalledWithValidAttributes() throws Exception {
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    DefaultMutableTreeNode child1 = new DefaultMutableTreeNode("child 1");
    DefaultMutableTreeNode child2 = new DefaultMutableTreeNode("child 2");

    parentNode.add(child1);
    parentNode.add(child2);

    int index = underTest.getIndexOfChild(parentNode, child2);
    assertEquals(index, 1);
  }

  @Test
  public void shouldReturnWithMinusOneWhenGetIndexOfChildCalledWithNotExistentChildNode() throws Exception {
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    DefaultMutableTreeNode child1 = new DefaultMutableTreeNode("child 1");
    DefaultMutableTreeNode child2 = new DefaultMutableTreeNode("child 2");

    parentNode.add(child1);
    parentNode.add(child2);
    DefaultMutableTreeNode anotherNode = new DefaultMutableTreeNode("anotherNode");

    int index = underTest.getIndexOfChild(parentNode, anotherNode);
    assertEquals(index, -1);
  }
}
