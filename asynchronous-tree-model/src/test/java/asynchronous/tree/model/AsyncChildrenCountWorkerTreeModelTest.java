package asynchronous.tree.model;

import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;
import asynchronous.tree.data.provider.ChildrenCountDataProvider;
import asynchronous.tree.node.DummyNode;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Test class for testing {@link AsyncChildrenCountWorkerTreeModel} class.
 */
public class AsyncChildrenCountWorkerTreeModelTest {

  private static final String ANY_NODE_NAME = "anyNodeName";

  private static final String ANY_PROVIDER_EXCEPTION_MESSAGE = "anyProviderErrorHappened";

  private static final Object SYNC_OBJECT = new Object();

  private ArgumentCaptor<ExceptionEvent> exceptionEventArgumentCaptor;

  @Mock
  private ChildrenCountDataProvider<TreeNode> dataProviderMock;

  @Mock
  private ExceptionListener exceptionListenerMock;

  @Mock
  private TreeModelListener treeModelListenerMock;

  AsyncChildrenCountWorkerTreeModel underTest;

  @BeforeMethod
  public void init() {
    MockitoAnnotations.initMocks(this);
    exceptionEventArgumentCaptor = ArgumentCaptor.forClass(ExceptionEvent.class);

    underTest = new AsyncChildrenCountWorkerTreeModel(dataProviderMock);
    underTest.addExceptionListener(exceptionListenerMock);
    underTest.addTreeModelListener(treeModelListenerMock);
  }

  @Test
  public void shouldReturnTrueWhenIsLeafCalledWithDummyNode() throws Exception {
    DummyNode dummyNode = new DummyNode();

    boolean isLeaf = underTest.isLeaf(dummyNode);

    assertTrue(isLeaf, "Dummy node is leaf.");
  }

  @Test
  public void shouldReturnTrueWhenIsLeafCalledWithLeafNodeAndTheBackendWorkerIsReady() throws Exception {
    // Given
    DefaultMutableTreeNode leafNode = new DefaultMutableTreeNode(ANY_NODE_NAME);

    when(dataProviderMock.getChildrenCount(leafNode)).thenReturn(leafNode.getChildCount());

    // When
    underTest.isLeaf(leafNode);
    verify(treeModelListenerMock, after(200).atLeastOnce()).treeStructureChanged(any());
    boolean isLeaf = underTest.isLeaf(leafNode);

    // Then
    assertTrue(isLeaf, "Should return true when isLeaf called with leaf node");
  }

  @Test
  public void shouldReturnFalseWhenIsLeafCalledWithParentNodeAndTheBackendWorkerIsReady() throws Exception {
    // Given
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);

    DefaultMutableTreeNode child1 = new DefaultMutableTreeNode("child1");
    DefaultMutableTreeNode child2 = new DefaultMutableTreeNode("child2");
    DefaultMutableTreeNode child3 = new DefaultMutableTreeNode("child3");

    parentNode.add(child1);
    parentNode.add(child2);
    parentNode.add(child3);

    when(dataProviderMock.getChildrenCount(parentNode)).thenReturn(parentNode.getChildCount());

    // When
    underTest.isLeaf(parentNode);
    verify(treeModelListenerMock, after(200).atLeastOnce()).treeStructureChanged(any());
    boolean isLeaf = underTest.isLeaf(parentNode);

    // Then
    assertFalse(isLeaf, "Should return false when isLeaf called with not a leaf node");
  }

  @Test
  public void shouldFireExceptionEventWhenIsLeafCalledAndSomethingWentWrongInTheBackendThread() throws Exception {
    // Given
    DefaultMutableTreeNode leafNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    when(dataProviderMock.getChildrenCount(leafNode)).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.isLeaf(leafNode);

    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(500);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getCause().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }

  @Test
  public void shouldZeroWhenGetChildCountCalledWithDummyNode() throws Exception {
    DummyNode dummyNode = new DummyNode();

    int numberOfChildren = underTest.getChildCount(dummyNode);

    assertEquals(numberOfChildren, 0, "Dummy node does not have children");
  }

  @Test
  public void shouldReturnZeroWhenGetChildCountCalledWithLeafNode() throws Exception {
    // Given
    DefaultMutableTreeNode leafNode = new DefaultMutableTreeNode(ANY_NODE_NAME);

    when(dataProviderMock.getChildrenCount(leafNode)).thenReturn(leafNode.getChildCount());

    // When
    underTest.getChildCount(leafNode);
    verify(treeModelListenerMock, after(200).atLeastOnce()).treeStructureChanged(any());
    int numberOfChildren = underTest.getChildCount(leafNode);

    // Then
    assertEquals(numberOfChildren, 0, "Leaf node does not have children");
  }

  @Test
  public void shouldReturnWithNumberOfTheChildrenWhenGetChildCountCalledWithParentNode() throws Exception {
    // Given
    DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(ANY_NODE_NAME);

    DefaultMutableTreeNode child1 = new DefaultMutableTreeNode("child1");
    DefaultMutableTreeNode child2 = new DefaultMutableTreeNode("child2");
    DefaultMutableTreeNode child3 = new DefaultMutableTreeNode("child3");

    parentNode.add(child1);
    parentNode.add(child2);
    parentNode.add(child3);

    when(dataProviderMock.getChildrenCount(parentNode)).thenReturn(parentNode.getChildCount());

    // When
    underTest.getChildCount(parentNode);
    verify(treeModelListenerMock, after(200).atLeastOnce()).treeStructureChanged(any());
    int numberOfChildren = underTest.getChildCount(parentNode);

    // Then
    assertEquals(numberOfChildren, 3);
  }

  @Test
  public void shouldFireExceptionEventWhenGetChildrenCountCalledAndSomethingWentWrongInTheBackendThread() throws Exception {
    // Given
    DefaultMutableTreeNode leafNode = new DefaultMutableTreeNode(ANY_NODE_NAME);
    when(dataProviderMock.getChildrenCount(leafNode)).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.getChildCount(leafNode);

    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(500);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getCause().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }
}
