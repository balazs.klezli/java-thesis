package asynchronous.tree.util;

import static org.testng.Assert.assertEquals;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import org.testng.annotations.Test;

public class TreeNodeUtilTest {

  private static final String NODE_NAME_ROOT = "root";
  private static final String NODE_NAME_FOR_FIRST_LEVEL_CHILD = "1st.LevelChild";
  private static final String NODE_NAME_FOR_SECOND_LEVEL_CHILD = "2nd.LevelChild";
  private static final String NODE_NAME_FOR_THIRD_LEVEL_CHILD = "3rd.LevelChild";

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void getPath_shouldThrowExceptionWhenCalledWithNull() {
    TreeNodeUtil.getPath(null);
  }

  @Test
  public void getPath_shouldReturnNullWhenCalledWithRootNode() {
    TreeNode anyTreeNode = new DefaultMutableTreeNode(NODE_NAME_ROOT);

    TreeNode[] result = TreeNodeUtil.getPath(anyTreeNode);

    assertEquals(result.length, 1);
    assertEquals(result[0].toString(), NODE_NAME_ROOT);
  }

  @Test
  public void getPath_shouldReturnOnlyTheRootInArrayWhenCalledWithARootChild() {
    DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(NODE_NAME_ROOT);
    DefaultMutableTreeNode childNodeA = new DefaultMutableTreeNode(NODE_NAME_FOR_FIRST_LEVEL_CHILD);
    rootNode.add(childNodeA);

    TreeNode[] result = TreeNodeUtil.getPath(childNodeA);

    assertEquals(result.length, 2);
    assertEquals(result[0].toString(), NODE_NAME_ROOT);
    assertEquals(result[1].toString(), NODE_NAME_FOR_FIRST_LEVEL_CHILD);
  }

  @Test
  public void getPath_shouldReturnCorrectPathInArrayWhenCalledWithDeepChild() {
    DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(NODE_NAME_ROOT);
    DefaultMutableTreeNode childNodeA = new DefaultMutableTreeNode(NODE_NAME_FOR_FIRST_LEVEL_CHILD);
    DefaultMutableTreeNode childNodeB = new DefaultMutableTreeNode(NODE_NAME_FOR_SECOND_LEVEL_CHILD);
    DefaultMutableTreeNode childNodeC = new DefaultMutableTreeNode(NODE_NAME_FOR_THIRD_LEVEL_CHILD);
    rootNode.add(childNodeA);
    childNodeA.add(childNodeB);
    childNodeB.add(childNodeC);

    TreeNode[] result = TreeNodeUtil.getPath(childNodeC);

    assertEquals(result.length, 4);
    assertEquals(result[0].toString(), NODE_NAME_ROOT);
    assertEquals(result[1].toString(), NODE_NAME_FOR_FIRST_LEVEL_CHILD);
    assertEquals(result[2].toString(), NODE_NAME_FOR_SECOND_LEVEL_CHILD);
    assertEquals(result[3].toString(), NODE_NAME_FOR_THIRD_LEVEL_CHILD);
  }
}
