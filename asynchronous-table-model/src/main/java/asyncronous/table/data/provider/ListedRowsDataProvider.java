package asyncronous.table.data.provider;

import java.util.List;

/**
 * Data rovider interface which extends {@link BaseDataProvider} and has a function for getting the table data in a list of rows.
 * @param <E> type of the rows
 */
public interface ListedRowsDataProvider<E extends Row> extends BaseDataProvider {

  /**
   * Returns the whole table data as a list of rows.
   * @return table data as list of rows
   * @throws java.lang.Exception when some error occurres during data retrieval
   */
  List<E> getListOfRows() throws Exception;
}
