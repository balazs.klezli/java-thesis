package asyncronous.table.data.provider;

import java.util.List;

/**
 * Base interface for table data providers.
 */
public interface BaseDataProvider {
  
  Object DUMMY_DATA = "";
  
  /**
   * @return collection of column names
   * @throws java.lang.Exception when some error occurres during column names retrieval
   */
  List<String> getColumnNames() throws Exception;

  /**
   * @return the row size of the data
   * @throws java.lang.Exception when some error occurres during row size retrieval
   */
  Integer getRowSize() throws Exception;

  /**
   * @return the column size of the data
   * @throws java.lang.Exception when some error occurres during column size retrieval
   */
  Integer getColumnSize() throws Exception;

}
