package asyncronous.table.data.provider;

/**
 * Data provider interface which extends {@link BaseDataProvider} and has function for getting a row based on the given index.
 * @param <E> type of the row
 */
public interface RowsDataProvider<E extends Row> extends BaseDataProvider {

  /**
   * Returns a row from the table data based on the given rowIndex
   * @param rowIndex the index of the row
   * @return a row from the table data
   * @throws java.lang.Exception when some error occurred during data retrieval
   */
  E getRow(int rowIndex) throws Exception;
}
