package asyncronous.table.data.provider;

public interface CellsDataProvider extends BaseDataProvider {

  /**
   * @param rowIndex index of the row
   * @param columnIndex index of the column
   * @return element from the table based on the given row and column index
   * @throws java.lang.Exception when some error occurres during cell data retrieval
   */
  Object getCellData(int rowIndex, int columnIndex) throws Exception;

}
