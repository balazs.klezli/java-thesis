package asyncronous.table.data.provider;

/**
 * Interface for a Row object
 */
public interface Row {

  /**
   * @return The count of the row elements
   */
  int valueCount();

  /**
   * Gets the row element based on the given index.
   * @param index the index of the element in the row
   * @return the row element
   */
  Object valueAt(int index);
}
