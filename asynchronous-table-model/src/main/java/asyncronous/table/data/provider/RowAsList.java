package asyncronous.table.data.provider;

import java.util.ArrayList;

/**
 * Row interface implementation which use an {@link ArrayList} object.
 * @param <E> type parameter of the row elements
 */
public class RowAsList<E> extends ArrayList<E> implements Row {

  @Override
  public int valueCount() {
    return this.size();
  }

  @Override
  public E valueAt(int index) {
    return this.get(index);
  }
}
