package asyncronous.table.data.provider;

/**
 * Row interface implementation which uses an array object.
 * @param <E> type parameter of the row elements
 */
public class RowAsArray<E> implements Row {

  private final E[] variable;
  
  public RowAsArray(E[] array) {
    this.variable = array;
  }
  
  @Override
  public int valueCount() {
    return variable.length;
  }

  @Override
  public E valueAt(int index) {
    return variable[index];
  }

}
