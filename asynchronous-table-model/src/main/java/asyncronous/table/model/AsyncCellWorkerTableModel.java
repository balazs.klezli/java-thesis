package asyncronous.table.model;

import asyncronous.table.data.provider.CellsDataProvider;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static asyncronous.table.data.provider.BaseDataProvider.DUMMY_DATA;

/**
 * Asynchronous implementation of {@link AbstractAsyncTableModel}. It can be used as an asynchronous model for a {@link JTable}.
 * The constructor method requires an implementation of the {@link CellsDataProvider} as a parameter.
 */
public class AsyncCellWorkerTableModel extends AbstractAsyncTableModel {

  private final Map<Integer, List<ElementAtSwingWorker>> elementsAtSwingWorkersTable = new HashMap<>();

  private final CellsDataProvider cellsDataProvider;

  public AsyncCellWorkerTableModel(CellsDataProvider cellsDataProvider) {
    super(cellsDataProvider);
    this.cellsDataProvider = cellsDataProvider;
  }
  
  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    List<ElementAtSwingWorker> elementAtSwingWorkerRow = elementsAtSwingWorkersTable.get(rowIndex);
    ElementAtSwingWorker elementAtSwingWorker = null;
    if (elementAtSwingWorkerRow != null && elementAtSwingWorkerRow.size() > columnIndex) {
      elementAtSwingWorker = elementAtSwingWorkerRow.get(columnIndex);
    }

    if (elementAtSwingWorker == null) {
      elementAtSwingWorker = new ElementAtSwingWorker(rowIndex, columnIndex);
      if(elementAtSwingWorkerRow == null) {
        elementAtSwingWorkerRow =  new ArrayList<>();
      } 
      elementAtSwingWorkerRow.add(elementAtSwingWorker);
      elementsAtSwingWorkersTable.put(rowIndex, elementAtSwingWorkerRow);
      elementAtSwingWorker.execute();
    }

    return elementAtSwingWorker.getValue();
  }

  @Override
  protected void invalidateWorkers() {
    elementsAtSwingWorkersTable.clear();
  }

  /**
   * {@link SwingWorker} implementation which retrieves a cell based on the given indices.
   * It uses the {@link CellsDataProvider}
   */
  class ElementAtSwingWorker extends SwingWorker<Object, Void> {
    
    private final int rowIndex, columnIndex;

    private Object value = null;

    public int getRowIndex() {
      return rowIndex;
    }

    public int getColumnIndex() {
      return columnIndex;
    }
    
    public Object getValue() {
      if (value == null){
        return DUMMY_DATA;
      }
      return value;
    }

    ElementAtSwingWorker(int rowIndex, int columnIndex) {
      this.rowIndex = rowIndex;
      this.columnIndex = columnIndex;
    }

    @Override
    protected Object doInBackground() throws Exception {
      return cellsDataProvider.getCellData(rowIndex, columnIndex);
    }

    @Override
    protected void done() {
      try {
        value = get();
      } catch (InterruptedException | ExecutionException e) {
        fireExceptionEvent(e);
      }
      fireTableCellUpdated(rowIndex, columnIndex);
    }

  }
}
