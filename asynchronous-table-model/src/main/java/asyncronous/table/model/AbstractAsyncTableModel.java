package asyncronous.table.model;

import asynchronous.exception.ExceptionHandler;
import asynchronous.exception.ExceptionHandlerImpl;
import asynchronous.exception.event.ExceptionListener;
import asyncronous.table.data.provider.BaseDataProvider;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static javax.swing.SwingWorker.StateValue.PENDING;

/**
 * Abstract asynchronous implementation of {@link AbstractTableModel}.
 */
public abstract class AbstractAsyncTableModel extends AbstractTableModel {

  private final ExceptionHandler eh = new ExceptionHandlerImpl();

  private RowSizeSwingWorker rowSizeSwingWorker = new RowSizeSwingWorker();

  private ColumnSizeSwingWorker columnSizeSwingWorker = new ColumnSizeSwingWorker();

  private ColumnNamesSwingWorker columnNamesSwingWorker = new ColumnNamesSwingWorker();

  /**
   * Data provider for the table model.
   */
  protected BaseDataProvider dataProvider;

  /**
   * Initializes the data provider.
   * @param dataProvider data provider for the model
   */
  protected AbstractAsyncTableModel(BaseDataProvider dataProvider) {
    this.dataProvider = dataProvider;
  }

  /**
   * Function to add an exception listener which will be informed when some error occurres in the data provider.
   * @param listener the listener which will be added to the exception listeners
   */
  public void addExceptionListener(ExceptionListener listener) {
    eh.addExceptionListener(listener);
  }

  /**
   * Function to remove an exception listener.
   * @param listener the listener which will be removed from the exception listeners
   */
  public void removeExceptionListener(ExceptionListener listener) {
    eh.removeExceptionListener(listener);
  }

  /**
   * Notifies the listeners about the exception.
   * @param exception the exception which will be sent to the listeners
   */
  protected void fireExceptionEvent(Exception exception) {
    eh.fireExceptionEvent(exception);
  }

  /**
   * Invalidates all SwingWorkers in the asynchronous table model implementations
   */
  protected abstract void invalidateWorkers();

  /**
   * Invalidates all data retrieved from the provider. This will cause the {@link JTable}s to be refreshed.
   */
  public void invalidateData() {
    refreshRowSizeSwingWorker();
    refreshColumnSizeSwingWorker();
    refreshColumnNamesSwingWorker();
    invalidateWorkers();
    fireTableStructureChanged();
  }

  private void refreshRowSizeSwingWorker() {
    rowSizeSwingWorker = new RowSizeSwingWorker();
  }

  private void refreshColumnSizeSwingWorker() {
    columnSizeSwingWorker = new ColumnSizeSwingWorker();
  }

  private void refreshColumnNamesSwingWorker() {
    columnNamesSwingWorker = new ColumnNamesSwingWorker();
  }

  @Override
  public int getColumnCount() {
    if (columnSizeSwingWorker.isInitialState()) {
      columnSizeSwingWorker.execute();
    }
    return columnSizeSwingWorker.getSize();
  }

  @Override
  public int getRowCount() {
    if (rowSizeSwingWorker.isInitialState()) {
      rowSizeSwingWorker.execute();
    }
    return rowSizeSwingWorker.getSize();
  }

  @Override
  public String getColumnName(int column) {
    if (columnNamesSwingWorker.isInitialState()) {
      columnNamesSwingWorker.execute();
    }

    if (columnNamesSwingWorker.getColumnNames().isEmpty()) {
      return "";
    }

    return columnNamesSwingWorker.getColumnNames().get(column).toString();
  }

  /**
   * SwingWorker implementation which retrieves the row size information based on the data provider.
   */
  class RowSizeSwingWorker extends SwingWorker<Integer, Void> {
    private int size = 0;

    public int getSize() {
      return size;
    }

    public boolean isInitialState() {
      return this.getState() == PENDING;
    }

    @Override
    protected Integer doInBackground() throws Exception {
      return dataProvider.getRowSize();
    }

    @Override
    protected void done() {
      try {
        size = get();
      } catch (InterruptedException | ExecutionException e) {
        fireExceptionEvent(e);
      }
      fireTableRowsInserted(0, size);
    }
  }


  /**
   * SwingWorker implementation which retrieves the column size information based on the data provider.
   */
  class ColumnSizeSwingWorker extends SwingWorker<Integer, Void> {
    private int size = 0;

    public int getSize() {
      return size;
    }

    public boolean isInitialState() {
      return this.getState() == PENDING;
    }

    @Override
    protected Integer doInBackground() throws Exception {
      return dataProvider.getColumnSize();
    }

    @Override
    protected void done() {
      try {
        size = get();
      } catch (InterruptedException | ExecutionException e) {
        fireExceptionEvent(e);
      }
      fireTableStructureChanged();
    }
  }


  /**
   * SwingWorker implementation which retrieves the column names based on the data provider.
   */
  class ColumnNamesSwingWorker extends SwingWorker<Collection<String>, Void> {
    private final List<String> columnNames = new ArrayList<>();

    public List<String> getColumnNames() {
      return columnNames;
    }

    public boolean isInitialState() {
      return this.getState() == PENDING;
    }

    @Override
    protected Collection<String> doInBackground() throws Exception {
      return dataProvider.getColumnNames();

    }

    @Override
    protected void done() {
      try {
        columnNames.addAll(get());
      } catch (InterruptedException e) {
        fireExceptionEvent(e);
      } catch (ExecutionException e) {
        fireExceptionEvent((Exception) e.getCause());
      }
      fireTableStructureChanged();
    }
  }
}
