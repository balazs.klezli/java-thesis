package asyncronous.table.model;

import asyncronous.table.data.provider.Row;
import asyncronous.table.data.provider.RowsDataProvider;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

import static asyncronous.table.data.provider.BaseDataProvider.DUMMY_DATA;
import java.util.concurrent.ExecutionException;

/**
 * Asynchronous implementation of {@link AbstractAsyncTableModel}. It can be used as an asynchronous model for a {@link JTable}.
 * The constructor method requires an implementation of the {@link RowsDataProvider} as a parameter.
 *
 * @param <E> generic type of the table rows
 */
public class AsyncRowWorkerTableModel<E extends Row> extends AbstractAsyncTableModel {

  private final List<RowSwingWorker> rowSwingWorkerList = new ArrayList<>();

  private final RowsDataProvider<E> rowsDataProvider;

  public AsyncRowWorkerTableModel(RowsDataProvider<E> rowsDataProvider) {
    super(rowsDataProvider);
    this.rowsDataProvider = rowsDataProvider;
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    RowSwingWorker rowSwingWorker = null;
    if (rowSwingWorkerList.size() - 1 >= rowIndex) {
      rowSwingWorker = rowSwingWorkerList.get(rowIndex);
    }

    if (rowSwingWorker == null) {
      rowSwingWorker = new RowSwingWorker(rowIndex);
      rowSwingWorkerList.add(rowIndex, rowSwingWorker);
      rowSwingWorker.execute();
    }

    if (rowSwingWorker.isDone()) {
      E row = rowSwingWorker.getRow();
      if (row != null && columnIndex < getColumnCount()) {
        return row.valueAt(columnIndex);
      }
    }

    return DUMMY_DATA;
  }

  @Override
  protected void invalidateWorkers() {
    rowSwingWorkerList.clear();
  }

  /**
   * {@link SwingWorker} implementation which retrieves a row based on the given rowIndex.
   * It uses the {@link RowsDataProvider}
   */
  class RowSwingWorker extends SwingWorker<E, Void> {

    private final int rowIndex;

    private E row;

    public int getRowIndex() {
      return rowIndex;
    }

    public RowSwingWorker(int rowIndex) {
      this.rowIndex = rowIndex;
    }

    public E getRow() {
      return row;
    }

    @Override
    protected E doInBackground() throws Exception {
      return rowsDataProvider.getRow(rowIndex);
    }

    @Override
    protected void done() {
      try {
        row = get();
      } catch (InterruptedException | ExecutionException e) {
        fireExceptionEvent(e);
      }
      fireTableRowsInserted(rowIndex, rowIndex);
    }

  }

}
