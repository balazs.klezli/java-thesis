package asyncronous.table.model;

import asyncronous.table.data.provider.Row;
import asyncronous.table.data.provider.ListedRowsDataProvider;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static asyncronous.table.data.provider.BaseDataProvider.DUMMY_DATA;
import static javax.swing.SwingWorker.StateValue.PENDING;

/**
 * Asynchronous implementation of {@link AbstractAsyncTableModel}. It can be used as an asynchronous model for a {@link JTable}.
 * The constructor method requires an implementation of the {@link ListedRowsDataProvider} as a parameter.
 *
 * @param <E> generic type of the rows int the table
 */
public class AsyncSingleWorkerTableModel<E extends Row> extends AbstractAsyncTableModel {

  private ElementsSwingWorker elementsSwingWorker = new ElementsSwingWorker();

  private final ListedRowsDataProvider<E> listedRowsDataProvider;

  public AsyncSingleWorkerTableModel(ListedRowsDataProvider<E> listedRowsDataProvider) {
    super(listedRowsDataProvider);
    this.listedRowsDataProvider = listedRowsDataProvider;
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    Object ret = DUMMY_DATA;
    if (elementsSwingWorker.isInitialState()) {
      elementsSwingWorker.execute();
    } else if (elementsSwingWorker.isDone()) {
      List<E> rows = elementsSwingWorker.getValues();
      if (!rows.isEmpty()) {
        E row = rows.get(rowIndex);
        if (row.valueCount() > columnIndex) {
          ret = row.valueAt(columnIndex);
        }
      }
    }

    return ret;
  }

  @Override
  protected void invalidateWorkers() {
    elementsSwingWorker = new ElementsSwingWorker();
  }

  /**
   * {@link SwingWorker} implementation which retrieves the list of the rows.
   * It uses the {@link ListedRowsDataProvider}.
   */
  class ElementsSwingWorker extends SwingWorker<List<E>, Void> {
    private List<E> values = new ArrayList<>();

    public List<E> getValues() {
      return values;
    }

    public boolean isInitialState() {
      return this.getState() == PENDING;
    }

    @Override
    protected List<E> doInBackground() throws Exception {
      return listedRowsDataProvider.getListOfRows();
    }

    @Override
    protected void done() {
      try {
        values = get();
      } catch (InterruptedException | ExecutionException e) {
        fireExceptionEvent(e);
      }
      fireTableDataChanged();
    }

  }
}
