package asyncronous.table.model;

import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;
import asyncronous.table.data.provider.CellsDataProvider;
import org.mockito.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static asyncronous.table.data.provider.BaseDataProvider.DUMMY_DATA;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class AsyncCellWorkerTableModelTest {

  private static final String ANY_PROVIDER_EXCEPTION_MESSAGE = "anyProviderErrorHappened";

  private static final Object SYNC_OBJECT = new Object();

  @Captor
  private ArgumentCaptor<ExceptionEvent> exceptionEventArgumentCaptor;

  @InjectMocks
  private AsyncCellWorkerTableModel underTest;

  @Mock
  private CellsDataProvider dataProviderMock;

  @Mock
  private ExceptionListener exceptionListenerMock;

  @BeforeMethod
  public void init() {
    MockitoAnnotations.initMocks(this);

    underTest = new AsyncCellWorkerTableModel(dataProviderMock);
    underTest.addExceptionListener(exceptionListenerMock);
  }

  @Test
  public void shouldReturnDummyDataWhenGetValueAtCalledWithoutWaiting() throws Exception {
    Object result = underTest.getValueAt(0, 0);

    assertEquals(result, DUMMY_DATA);
  }

  @Test
  public void shouldReturnValidValueWhenGetValueAtCalledAndWaitForBackendThread() throws Exception {
    // When
    String cellData = "anyData";

    when(dataProviderMock.getCellData(0, 0)).thenReturn(cellData);

    // When
    await()
            .atMost(5, SECONDS)
            .until(() -> underTest.getValueAt(0, 0) != DUMMY_DATA);
    String actualData = (String) underTest.getValueAt(0, 0);

    // Then
    assertEquals(actualData, cellData);
  }

  @Test
  public void shouldFireExceptionEventWhenGetValueAtCalledAndSomethingWentWrongInTheBackgroundThread() throws Exception {
    // Given
    when(dataProviderMock.getCellData(0, 0)).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.getValueAt(0, 0);
    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(200);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getCause().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }
}
