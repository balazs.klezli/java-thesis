package asyncronous.table.model;

import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;
import asyncronous.table.data.provider.RowAsList;
import asyncronous.table.data.provider.ListedRowsDataProvider;
import org.mockito.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static asyncronous.table.data.provider.BaseDataProvider.DUMMY_DATA;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class AsyncSingleWorkerTableModelTest {

  private static final String ANY_PROVIDER_EXCEPTION_MESSAGE = "anyProviderErrorHappened";

  private static final Object SYNC_OBJECT = new Object();

  @Captor
  private ArgumentCaptor<ExceptionEvent> exceptionEventArgumentCaptor;

  @InjectMocks
  private AsyncSingleWorkerTableModel<RowAsList<String>> underTest;

  @Mock
  private ListedRowsDataProvider<RowAsList<String>> dataProviderMock;

  @Mock
  private ExceptionListener exceptionListenerMock;

  @BeforeMethod
  public void init() {
    MockitoAnnotations.initMocks(this);

    underTest = new AsyncSingleWorkerTableModel(dataProviderMock);
    underTest.addExceptionListener(exceptionListenerMock);
  }

  @Test
  public void shouldReturnNullWhenGetValueAtCalledWithoutWaiting() throws Exception {
    Object result = underTest.getValueAt(0, 0);

    assertEquals(result, DUMMY_DATA, "getValueAt without waiting returns with dummy data");
  }

  @Test
  public void shouldReturnValidValueWhenGetValueAtCalledAndWaitForBackendThread() throws Exception {
    // When
    int numberOfColumns = new Random().nextInt(10);
    List<RowAsList<String>> expectedRow = new ArrayList<>();
    expectedRow.add(new RowAsList<>());
    IntStream.range(0, numberOfColumns).forEach(i -> expectedRow.get(0).add(getDataWithIndex(i)));

    when(dataProviderMock.getListOfRows()).thenReturn(expectedRow);

    // When
    await()
            .atMost(5, SECONDS)
            .until(() -> underTest.getValueAt(0, 0) != DUMMY_DATA);
    List<String> actualData = new ArrayList<>();
    IntStream
            .range(0, numberOfColumns)
            .forEach(i -> actualData.add((String) underTest.getValueAt(0, i)));

    // Then
    IntStream
            .range(0, numberOfColumns)
            .forEach(i -> assertEquals(actualData.get(i), expectedRow.get(0).get(i)));
  }

  @Test
  public void shouldFireExceptionEventWhenGetValueAtCalledAndSomethingWentWrongInTheBackgroundThread() throws Exception {
    // Given
    when(dataProviderMock.getListOfRows()).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.getValueAt(0, 0);
    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(200);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getCause().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }

  private String getDataWithIndex(int index) {
    return "anyData" + index;
  }
}
