package asyncronous.table.model;

import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;
import asyncronous.table.data.provider.RowAsArray;
import asyncronous.table.data.provider.ListedRowsDataProvider;
import org.mockito.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

/**
 * Test class for testing common parts of the table model.
 * It uses {@link AsyncSingleWorkerTableModel} because the common class is abstract
 */
public class AbstractAsyncTableModelTest {

  private static final String ANY_PROVIDER_EXCEPTION_MESSAGE = "anyProviderErrorHappened";

  private static final Object SYNC_OBJECT = new Object();

  @Captor
  private ArgumentCaptor<ExceptionEvent> exceptionEventArgumentCaptor;

  @InjectMocks
  private AsyncSingleWorkerTableModel<RowAsArray<String>> underTest;

  @Mock
  private ListedRowsDataProvider dataProviderMock;

  @Mock
  private ExceptionListener exceptionListenerMock;

  @BeforeMethod
  public void init() {
    MockitoAnnotations.initMocks(this);

    underTest = new AsyncSingleWorkerTableModel(dataProviderMock);
    underTest.addExceptionListener(exceptionListenerMock);
  }

  @Test
  public void shouldReturnNumberOfColumnsWhenGetColumnCountCalled() throws Exception {
    int expectedNumberOfColumns = new Random().nextInt(25);
    when(dataProviderMock.getColumnSize()).thenReturn(expectedNumberOfColumns);

    await().atMost(5, SECONDS).until(() -> underTest.getColumnCount() != 0);
    int numberOfColumns = underTest.getColumnCount();

    assertEquals(numberOfColumns, expectedNumberOfColumns);
  }

  @Test
  public void shouldFireExceptionEventWhenGetColumnCountCalledAndSomethingWentWrongInTheBackgroundThread() throws Exception {
    // Given
    when(dataProviderMock.getColumnSize()).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.getColumnCount();
    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(200);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getCause().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }

  @Test
  public void shouldReturnNumberOfRowsWhenGetRowCountCalled() throws Exception {
    int expectedNumberOfRows = new Random().nextInt(25);
    when(dataProviderMock.getRowSize()).thenReturn(expectedNumberOfRows);

    await().atMost(5, SECONDS).until(() -> underTest.getRowCount() != 0);
    int numberOfRows = underTest.getRowCount();

    assertEquals(numberOfRows, expectedNumberOfRows);
  }

  @Test
  public void shouldFireExceptionEventWhenGetRowCountCalledAndSomethingWentWrongInTheBackgroundThread() throws Exception {
    // Given
    when(dataProviderMock.getRowSize()).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.getRowCount();
    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(200);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getCause().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }

  @Test
  public void shouldReturnEmptyStringWhenGetColumnNameCalledWithoutWaiting() throws Exception {
    int indexOfColumn = new Random().nextInt(25);
    String actualColumnName = underTest.getColumnName(indexOfColumn);

    // Then
    assertEquals(actualColumnName, "");
  }

  @Test
  public void shouldReturnColumnNamesWhenGetColumnNameCalledAndWaitForBackendThread() throws Exception {
    // Given
    int numberOfElements = 10;
    List<String> expectedColumnNames = new ArrayList<>();
    IntStream
            .range(0, numberOfElements)
            .forEach(i -> expectedColumnNames.add(getNameWithIndex(i)));

    when(dataProviderMock.getColumnNames()).thenReturn(expectedColumnNames);

    // When
    await()
            .atMost(5, SECONDS)
            .until(() -> !"".equals(underTest.getColumnName(2)));
    List<String> actualColumnNames = new ArrayList<>();
    IntStream
            .range(0, numberOfElements)
            .forEach(i -> actualColumnNames.add(underTest.getColumnName(i)));

    // Then
    IntStream
            .range(0, numberOfElements)
            .forEach(i -> assertEquals(actualColumnNames.get(i), expectedColumnNames.get(i)));
  }

  @Test
  public void shouldFireExceptionEventWhenGetColumnNameCalledAndSomethingWentWrongInTheBackgroundThread() throws Exception {
    // Given
    int indexOfColumn = new Random().nextInt(25);
    when(dataProviderMock.getColumnNames()).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.getColumnName(indexOfColumn);
    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(200);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }

  private String getNameWithIndex(int index) {
    return "name" + index;
  }
}
