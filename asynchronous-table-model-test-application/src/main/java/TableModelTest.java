import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.SOUTH;
import static java.awt.EventQueue.invokeLater;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;
import asyncronous.table.model.AbstractAsyncTableModel;
import asyncronous.table.model.AsyncSingleWorkerTableModel;
import asyncronous.table.model.AsyncCellWorkerTableModel;
import asyncronous.table.model.AsyncRowWorkerTableModel;
import data.provider.ModelType;
import data.provider.helper.Helper;
import data.provider.internet.FacebookRowDataProvider;
import data.provider.internet.FootballLeagueWinnerCellDataProvider;
import data.provider.internet.OpenShiftRowDataProvider;
import data.provider.internet.SingleWorkerInternetJsonDataProvider;
import data.provider.internet.SingleWorkerInternetXmlDataProvider;
import data.provider.local.SingleWorkerLocalJsonDataProvider;
import data.provider.local.SingleWorkerLocalXmlDataProvider;

public class TableModelTest extends JFrame implements ActionListener {

  private static final String XML_FILE_INTERNET_PATH = "http://www.cs.washington.edu/research/projects/xmltk/xmldata/data/mondial/mondial-3.0_pv.xml";
  private static final String INVALIDATE_BTN_TEXT = "invalidate data";
  private static final String ACTION_INVALIDATE = "invalidate";

  private AbstractAsyncTableModel model;
  private final JTable table;
  private JTextArea exceptionsArea;
  private JButton invalidateBtn;
  private final Helper helper = new Helper();
  private static String facebookToken = "";

  public static void main(String s[]) {
    invokeLater(() -> new TableModelTest().setVisible(true));
  }

  public TableModelTest() throws HeadlessException {
    setLayout(new BorderLayout());
    setSize(1200, 400);
    setLocationRelativeTo(null);
    setTitle("Table Model Example");
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    table = new JTable();
    getContentPane().add(new JScrollPane(table), CENTER);
    fillBottomContainer(getBottomContainer());

    final JToolBar toolBar = new JToolBar();
    getContentPane().add(toolBar, BorderLayout.NORTH);
    for (ModelType modelType : ModelType.values()) {
      toolBar.add(new NewModelAction(modelType));
    }
  }

  private class NewModelAction extends AbstractAction {
    private final ModelType modelType;

    public NewModelAction(ModelType modelType) {
      super(modelType.toString());
      this.modelType = modelType;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      switch (modelType) {
        case LocalSingleWorkerXml:
          model = new AsyncSingleWorkerTableModel<>(new SingleWorkerLocalXmlDataProvider(null));
          break;
        case LocalSingleWorkerJson:
          model = new AsyncSingleWorkerTableModel<>(new SingleWorkerLocalJsonDataProvider(helper));
          break;
        case InternetRowWorkerOpenShift:
          try {
            model = new AsyncRowWorkerTableModel<>(new OpenShiftRowDataProvider(helper));
          } catch (Exception ex) {
            ex.printStackTrace(System.err);
            exceptionsArea.append(ex.getMessage());
            exceptionsArea.append("\n");
          }
          break;
        case InternetRowWorkerFaceBook:

          if (!isFacebookTokenInitialized()) {
            showFacebookTokenDialog();
          }

          if (isFacebookTokenInitialized()) {
            model = new AsyncRowWorkerTableModel<>(new FacebookRowDataProvider(facebookToken, helper));
          }
          break;
        case InternetSingleWorkerXml:
          model = new AsyncSingleWorkerTableModel<>(new SingleWorkerInternetXmlDataProvider(XML_FILE_INTERNET_PATH, helper));
          break;
        case InternetSingleWorkerJson:
          model = new AsyncSingleWorkerTableModel<>(new SingleWorkerInternetJsonDataProvider(helper));
          break;
        case InternetCellFootballWinner:
          model = new AsyncCellWorkerTableModel(new FootballLeagueWinnerCellDataProvider(helper));
          break;
      }

      if(model != null) {
        model.addExceptionListener(new TableModelExceptionListener());
        invalidateBtn.setEnabled(true);
        table.setModel(model);
      }
    }
  }

  private boolean isFacebookTokenInitialized() {
    return !"".equals(facebookToken);
  }

  private void showFacebookTokenDialog() {
    JDialog modalDialog = new JDialog(this, "Set facebook token", true);
    modalDialog.setLocationRelativeTo(this);
    modalDialog.setSize(350, 80);
    modalDialog.setLayout(new FlowLayout());
    modalDialog.setResizable(false);

    modalDialog.add(new JLabel("Token:"));
    final JTextField textField = new JTextField(20);
    modalDialog.add(textField);

    JButton okButton = new JButton("Set");
    modalDialog.add(okButton);
    okButton.addActionListener(
            e -> {
              facebookToken = textField.getText();
              modalDialog.setVisible(false);
              modalDialog.dispose();
            }
    );
    modalDialog.setVisible(true);
  }

  private void fillBottomContainer(Container bottomContainer) {
    invalidateBtn = new JButton(INVALIDATE_BTN_TEXT);
    invalidateBtn.addActionListener(this);
    invalidateBtn.setActionCommand(ACTION_INVALIDATE);
    bottomContainer.add(invalidateBtn, CENTER);

    exceptionsArea = new JTextArea("List of occurred exceptions:\n\n");
    JScrollPane scrollPane = new JScrollPane(exceptionsArea);
    scrollPane.setPreferredSize(new Dimension(scrollPane.getWidth(), 100));
    bottomContainer.add(scrollPane, SOUTH);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (ACTION_INVALIDATE.equals(e.getActionCommand())) {
      model.invalidateData();
    }
  }

  private Container getBottomContainer() {
    Container c = new Container();
    c.setLayout(new BorderLayout());
    add(c, SOUTH);
    return c;
  }

  protected class TableModelExceptionListener implements ExceptionListener {

    @Override
    public void exceptionReceived(ExceptionEvent event) {
      final Exception exceptionHappened = event.getException();
      SwingUtilities.invokeLater(() -> {
        exceptionsArea.append("**** Beginning of exception description\n");
        exceptionsArea.append("Class of the exception: " + exceptionHappened.getClass() + "\n");
        exceptionsArea.append("Message of the exception: " + exceptionHappened.getMessage() + "\n");
        exceptionsArea.append("**** End of exception description\n\n");
        exceptionHappened.printStackTrace(System.err);
      });
    }

  }
}
