package data.provider.internet;

import java.io.IOException;

import data.provider.JsonDataProvider;
import data.provider.helper.Helper;

public class SingleWorkerInternetJsonDataProvider extends JsonDataProvider {

  private static final String JSON_URL = "http://ropman.web.elte.hu/thesis/MOCK_DATA_FROM_MOCKAROO.json";

  private Helper helper = null;

  public SingleWorkerInternetJsonDataProvider(Helper helper) {
    this.helper = helper;
  }

  @Override
  protected String getJsonString() throws IOException {
    return helper.jsonStringFromUrl(JSON_URL);
  }
}
