package data.provider.internet;

import asyncronous.table.data.provider.RowAsList;
import asyncronous.table.data.provider.RowsDataProvider;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import data.provider.helper.Helper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class FacebookRowDataProvider implements RowsDataProvider<RowAsList<String>> {

  private Helper helper = null;

  public FacebookRowDataProvider(String token, Helper helper) {
    this.token = token;
    this.helper = helper;
  }

  public FacebookRowDataProvider(Helper helper) {
    this.helper = helper;
  }

  // this token is valid for only an hour
  // get from https://developers.facebook.com/tools/explorer/145634995501895/?method=GET&path=cocacola%3Ffields%3Dfan_count&version=v2.7
  private String token = null;

  protected static final String TEMPORARY_OAUTH_TOKEN = "EAACEdEose0cBACrXeeHsnjMYJbJrNaBWoGTe93bszMTHrliXOK1RpTLZBMXGVsqaUTTZCeY1zLTFbPmZB2K7aELFDxTYjiuqEHrwZBfFozO52LW3FZA51qoVGZBn8RE9gWooNJCz9v2MJVlArOtmW1lHZAZBatZCZAK2m2NRv3Czd9CAZDZD";

  protected static final String URL_FACEBOOK_GRAPH_API_PREFIX = "https://graph.facebook.com/";
  protected static final String VERSION_2_7 = "v2.7";
    protected static final String[] FORTY_PAGE_NAMES = {"cocacolahu", "mcdonaldshungary", "LumiaSegitseg",
          "redbull", "SamsungMobile", "usnikefootball", "oreo", "kfcmagyarorszag", "yoamoloszapatos", "PlayStation",
          "converse", "BestByte", "pepsi", "Nescafe.HU", "walmart", "Nutella", "iTunes", "Skype",
          "adidasoriginals", "PizzaHut", "hmhungaryhm", "BlackBerry", "VolkswagenMagyarorszag", "victoriassecret",
          "Amazon", "DoveMagyarorszag", "netflix", "KitKatMagyarorszag", "Pringles", "adidas", "MonsterEnergy",
          "Zara", "sonymobile", "skittles", "LOrealParisHungary", "target", "SamsungMobileUSA", "subway",
          "Intel", "nike"};
  protected static final String[] COLUMN_NAMES = {"id", "name", "link", "about", "fan_count"};
  protected static final String FILEDS_PREFIX = "fields=";
  protected static final String ACCESS_TOKEN_PART = "access_token=";

  /**
   * It retrieves a json data like this: { "id": "1563838817166050", "name":
   * "Coca-Cola", "link": "https://www.facebook.com/cocacolahu/", "about": "The
   * Coca-Cola Facebook Page is a collection of your stories showing how people
   * from around the world have helped make Coke into what it is today." }
   *
   * @throws IOException
   */

  @Override
  public RowAsList<String> getRow(int rowIndex) throws IOException {
    RowAsList<String> row = new RowAsList<>();

    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < getColumnSize(); ++i) {
      sb.append(COLUMN_NAMES[i]);
      sb.append(",");
    }
    String columns = sb.toString();
    columns = columns.substring(0, columns.length() - 1);

    String urlPath = URL_FACEBOOK_GRAPH_API_PREFIX + VERSION_2_7 + "/" + FORTY_PAGE_NAMES[rowIndex] + "?" +
            FILEDS_PREFIX + columns + "&" + ACCESS_TOKEN_PART;
    urlPath += token != null ? token : TEMPORARY_OAUTH_TOKEN;

    String json = helper.jsonStringFromUrl(urlPath);

    JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
    for (int i = 0; i < getColumnSize(); ++i) {
      row.add(i, jsonObject.get(COLUMN_NAMES[i]).getAsString());
    }

    return row;
  }

  @Override
  public List<String> getColumnNames() {
    return Arrays.asList(COLUMN_NAMES);
  }

  @Override
  public Integer getRowSize() {
    return FORTY_PAGE_NAMES.length;
  }

  @Override
  public Integer getColumnSize() {
    return COLUMN_NAMES.length;
  }
}
