package data.provider.internet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import data.provider.helper.Helper;
import org.apache.commons.lang3.StringEscapeUtils;

import asyncronous.table.data.provider.CellsDataProvider;

/**
 * http://www.focikatalogus.hu/bajnoksagok
 * https://www.mediawiki.org/wiki/API:Main_page
 * 
 * Gathering data from different wiki pages like ( DFL-Supercup,
 * Supercoppa_Italiana, .. ) for winners in different years.
 * 
 * There are years in the rows and different football leagues in the columns
 */
public class FootballLeagueWinnerCellDataProvider implements CellsDataProvider {

  /**
   *  description: DFL-Supercup | DFL-Supercup
   *  url: https://en.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=content&titles=DFL-Supercup
   *  regexp pattern: ((DFB\-Supercup\|)(\d+)(\]\]\\n\| '''\[\[)([^\|]+))
   *  fixed regexp pattern: ((DFB\-Supercup\||DFL\-Supercup\|)(\d+)([^\[]+\[\[)([^\||^\]]+))
   *  normalized pattern: ((DFB\\-Supercup\\||DFL\\-Supercup\\|)(\\d+)([^\\[]+\\[\\[)([^\\||^\\]]+))
   *  
   *  description: Supercoppa Italiana
   *  url: https://en.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=content&titles=Supercoppa_Italiana
   *  regexp pattern: ((Supercoppa Italiana\|)(\d+)(\]\]\\n\|style\=\\"background:#cfecec;\\" align=center\|'''\[\[)([^\|]+))
   *  normalized pattern: ((Supercoppa Italiana\\|)(\\d+)(\\]\\]\\\\n\\|style\\=\\\\\"background:#cfecec;\\\\\" align=center\\|'''\\[\\[)([^\\|]+))
   *  
   *  https://regex101.com/r/dB6gS0/4
   *  description: Supercopa_de_España
   *  url: https://en.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=content&titles=Supercopa_de_Espa%C3%B1a
   *  regexp pattern: ((Supercopa de Espa\\u00f1a\|)(\d+)(\]\]\\n\| '''\[\[)([^\||^\]|^\\]+))
   *  fixed regexp pattern: ((Supercopa de Espa\\u00f1a\|)(1991)(\]\]\\n\| '''\[\[)([^\||^\]|^\\]+|[^\]]+))
   *                        ((Supercopa de Espa\\u00f1a\|)(2014)(\]\]\\n\| '''\[\[)([^\||^\]]+|[^\\]|[^\\]+))
   *  normalized pattern: ((Supercopa de Espa\\\\u00f1a\\|)(\\d+)(\\]\\]\\\\n\\| '''\\[\\[)([^\\||^\\]]+|[^\\\\]+))
   *
   *  */

  private static final String URL_PREFIX = "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=content&titles=";

  private static final String[] LEAGUES = { "YEARS", "DFB-Supercup | DFL-Supercup", "Supercoppa_Italiana", "Supercopa_de_España" };

  private static final String[] PATTERNS_FOR_LEAGUES_WIKI_PAGES = { "", "((DFB\\-Supercup\\||DFL\\-Supercup\\|)(\\d+)([^\\[]+\\[\\[)([^\\||^\\]]+))",
      "((Supercoppa Italiana\\|)(\\d+)(\\]\\]\\\\n\\|style\\=\\\\\"background:#cfecec;\\\\\" align=center\\|'''\\[\\[)([^\\|]+))",
      "((Supercopa de Espa\\\\u00f1a\\|)(\\d+)(\\]\\]\\\\n\\| '''\\[\\[)([^\\||^\\]]+|[^\\\\]+))" };

  private final List<String> years;

  private Helper helper = null;

  private static final int YEAR_COLUMN_INDEX = 0;

  public FootballLeagueWinnerCellDataProvider(Helper helper) {
    this.helper = helper;
    years = new ArrayList<>();
    int firstYear = 1980;
    int lastYear = new GregorianCalendar().get(Calendar.YEAR);
    for (int i = firstYear; i <= lastYear; ++i) {
      years.add(i+"");
    }
  }

  @Override
  public Object getCellData(int rowIndex, int columnIndex) throws Exception {
    if (columnIndex == YEAR_COLUMN_INDEX) {
      return years.get(rowIndex);
    }

    String urlPath;
    if (columnIndex == 1) {
      urlPath = URL_PREFIX + "DFL-Supercup";
    } else {
      urlPath = URL_PREFIX + LEAGUES[columnIndex];
    }
    String json = helper.jsonStringFromUrl(urlPath);

    String pattern = PATTERNS_FOR_LEAGUES_WIKI_PAGES[columnIndex];
    Pattern r = Pattern.compile(pattern);
    Matcher m = r.matcher(json);
    Object result = DUMMY_DATA;
    while (m.find()) {
      if (years.get(rowIndex).equals(m.group(3))) {
        result = StringEscapeUtils.unescapeJava(m.group(5));
      }
    }

    return result;
  }

  @Override
  public List<String> getColumnNames() throws Exception {
    return Arrays.asList(LEAGUES);
  }

  @Override
  public Integer getRowSize() throws Exception {
    return years.size();
  }

  @Override
  public Integer getColumnSize() throws Exception {
    return LEAGUES.length;
  }

}
