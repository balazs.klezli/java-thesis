package data.provider.internet;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import data.provider.NodeListBasedTableBaseDataProvider;
import data.provider.helper.Helper;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import asyncronous.table.data.provider.ListedRowsDataProvider;
import asyncronous.table.data.provider.RowAsArray;

public class SingleWorkerInternetXmlDataProvider extends NodeListBasedTableBaseDataProvider implements ListedRowsDataProvider<RowAsArray<Object>> {

  private static final String URL_STRING = "http://www.cs.washington.edu/research/projects/xmltk/xmldata/data/mondial/mondial-3.0_pv.xml";

  private String urlPath;

  private Helper helper = null;

  public SingleWorkerInternetXmlDataProvider(Helper helper) {
    this.helper = helper;
  }

  public SingleWorkerInternetXmlDataProvider(String urlPath, Helper helper) {
    this.urlPath = urlPath;
    this.helper = helper;
  }

  @Override
  protected NodeList getData() throws IOException, SAXException, ParserConfigurationException {
    String url;
    if (urlPath != null) {
      url = urlPath;
    } else {
      url = URL_STRING;
    }
    InputStream inputStream = helper.inputStreamFromUrl(url);
    Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);

    return doc.getElementsByTagName("country");
  }

  @Override
  public List<RowAsArray<Object>> getListOfRows() throws IOException, SAXException, ParserConfigurationException {
    NodeList dataTable = getData();
    List<RowAsArray<Object>> listOfArrays = new ArrayList<>();

    int numberOfRows = dataTable.getLength();
    while (numberOfRows-- > 0) {
      listOfArrays.add(getRowAsArray(dataTable, numberOfRows));
    }

    return listOfArrays;
  }
}
