package data.provider.internet;

import java.util.Arrays;
import java.util.List;

import asyncronous.table.data.provider.RowAsList;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import asyncronous.table.data.provider.RowsDataProvider;
import data.provider.helper.Helper;

public class OpenShiftRowDataProvider implements RowsDataProvider<RowAsList<Object>> {

  private static String[] columnNames;

  private Helper helper = null;

  private static final String URL_GET_COLUMN_NAMES = "http://thesis-bklezli.rhcloud.com/rest/members/columnNames";
  private static final String URL_PART_GET_MEMBERS = "http://thesis-bklezli.rhcloud.com/rest/members/";
  private static final String URL_GET_NUMBER_OF_MEMBERS = "http://thesis-bklezli.rhcloud.com/rest/members/number";

  public OpenShiftRowDataProvider(Helper helper) throws Exception {
    this.helper = helper;
    JsonArray jsonArray = helper.jsonElementFromUrl(URL_GET_COLUMN_NAMES).getAsJsonArray();
    columnNames = new String[jsonArray.size()];
    for (int i = 0; i < jsonArray.size(); i++) {
      columnNames[i] = jsonArray.get(i).getAsString();
    }
  }

  @Override
  public RowAsList<Object> getRow(int rowIndex) throws Exception {
    RowAsList<Object> row = new RowAsList<>();
    JsonObject jsonObject = helper.jsonElementFromUrl(URL_PART_GET_MEMBERS + rowIndex).getAsJsonObject();
    for (int i = 0; i < getColumnSize(); ++i) {
      row.add(jsonObject.get(getColumnNames().get(i)));
    }

    return row;
  }

  @Override
  public List<String> getColumnNames() throws Exception {
    return Arrays.asList(columnNames);
  }

  @Override
  public Integer getRowSize() throws Exception {
    return helper.jsonElementFromUrl(URL_GET_NUMBER_OF_MEMBERS).getAsInt();
  }

  @Override
  public Integer getColumnSize() throws Exception {
    return getColumnNames().size();
  }

}
