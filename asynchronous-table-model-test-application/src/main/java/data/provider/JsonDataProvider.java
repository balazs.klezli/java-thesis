package data.provider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import asyncronous.table.data.provider.ListedRowsDataProvider;
import asyncronous.table.data.provider.RowAsArray;

public abstract class JsonDataProvider implements ListedRowsDataProvider<RowAsArray<String>> {

  protected static final String[] COLUMN_NAMES = { "id", "first_name", "last_name", "email", "gender", "ip_address" };

  @Override
  public List<RowAsArray<String>> getListOfRows() throws Exception {
    List<RowAsArray<String>> rows = new ArrayList<>();

    String json = getJsonString();
    JsonArray jsonArray = new JsonParser().parse(json).getAsJsonArray();

    for (int i = 0; i < jsonArray.size(); ++i) {
      JsonObject obj = jsonArray.get(i).getAsJsonObject();
      String[] arr = new String[COLUMN_NAMES.length];
      for (int j = 0; j < COLUMN_NAMES.length; j++) {
        arr[j] = obj.get(COLUMN_NAMES[j]).getAsString();
      }
      rows.add(new RowAsArray<>(arr));
    }

    return rows;
  }

  @Override
  public List<String> getColumnNames() throws Exception {
    return Arrays.asList(COLUMN_NAMES);
  }

  @Override
  public Integer getRowSize() throws Exception {
    return getListOfRows().size();
  }

  @Override
  public Integer getColumnSize() throws Exception {
    return COLUMN_NAMES.length;
  }

  protected abstract String getJsonString() throws Exception;

}
