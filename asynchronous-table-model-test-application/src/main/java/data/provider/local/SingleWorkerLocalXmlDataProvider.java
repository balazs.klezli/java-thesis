package data.provider.local;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import data.provider.NodeListBasedTableBaseDataProvider;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import asyncronous.table.data.provider.ListedRowsDataProvider;
import asyncronous.table.data.provider.RowAsArray;

public class SingleWorkerLocalXmlDataProvider extends NodeListBasedTableBaseDataProvider implements ListedRowsDataProvider<RowAsArray<Object>> {

  private static final String RELATIVE_PATH = "/mondial-3.0_pv.xml";

  private String relativePath = null;

  public SingleWorkerLocalXmlDataProvider() {
  }

  public SingleWorkerLocalXmlDataProvider(String relativePath) {
    this.relativePath = relativePath;
  }

  @Override
  protected NodeList getData() throws InterruptedException, SAXException, ParserConfigurationException, IOException {
    // simulating heavy calculation
    Thread.sleep(500);
    InputStream inputStream;
    if (relativePath != null) {
      inputStream = new FileInputStream(relativePath);
    } else {
      inputStream = getClass().getResourceAsStream(RELATIVE_PATH);
    }
    Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);

    return doc.getElementsByTagName("country");
  }

  @Override
  public List<RowAsArray<Object>> getListOfRows() throws InterruptedException, SAXException, IOException, ParserConfigurationException {
    NodeList dataTable = getData();
    List<RowAsArray<Object>> listOfArrays = new ArrayList<>();

    int numberOfRows = dataTable.getLength();
    while (numberOfRows-- > 0) {
      listOfArrays.add(getRowAsArray(dataTable, numberOfRows));
    }

    return listOfArrays;
  }
}
