package data.provider.local;

import java.io.InputStream;

import data.provider.JsonDataProvider;
import data.provider.helper.Helper;

public class SingleWorkerLocalJsonDataProvider extends JsonDataProvider {

  private static final String RELATIVE_PATH = "/MOCK_DATA_FROM_MOCKAROO.json";

  private Helper helper = null;

  public SingleWorkerLocalJsonDataProvider(Helper helper) {
    this.helper = helper;
  }

  @Override
  protected String getJsonString() throws Exception {
    Thread.sleep(500);
    InputStream inputStream = getClass().getResourceAsStream(RELATIVE_PATH);
    return helper.getStringFromInputStream(inputStream);

  }
}
