package data.provider.helper;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Helper {

  public String getStringFromInputStream(InputStream is) throws IOException {

    BufferedReader br = null;
    StringBuilder sb = new StringBuilder();

    String line;
    try {

      br = new BufferedReader(new InputStreamReader(is));
      while ((line = br.readLine()) != null) {
        sb.append(line);
      }

    } finally {
      if (br != null) {
        br.close();
      }
    }

    return sb.toString();
  }

  public InputStream inputStreamFromUrl(String urlString) throws IOException {
    URL url = new URL(urlString);
    URLConnection connection = url.openConnection();
    return connection.getInputStream();
  }

  public String jsonStringFromUrl(String url) throws IOException {
    return getStringFromInputStream(inputStreamFromUrl(url));
  }

  public JsonElement jsonElementFromUrl(String url) throws Exception {
    String json = jsonStringFromUrl(url);
    return new JsonParser().parse(json);
  }
}
