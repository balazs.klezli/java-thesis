package data.provider;

import asyncronous.table.data.provider.RowAsArray;
import asyncronous.table.data.provider.BaseDataProvider;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Abstract implementation of {@link BaseDataProvider} which uses {@link NodeList} as a data source.
 */
public abstract class NodeListBasedTableBaseDataProvider implements BaseDataProvider {

  /**
   * @return the tree data as a {@link NodeList}
   * @throws Exception when some error occurres during data retrieval
   */
  protected abstract NodeList getData() throws Exception;
  
  @Override
  public List<String> getColumnNames() throws Exception {
    return getSortedColumnNames(getData());
  }

  @Override
  public Integer getRowSize() throws Exception {
    return getData().getLength();
  }

  @Override
  public Integer getColumnSize() throws Exception {
    return getColumnSize(getData());
  }

  protected Integer getColumnSize(NodeList dataTable) {
    return getRowFromTable(dataTable, 0).getLength();
  }
  
  protected NamedNodeMap getRowFromTable(NodeList dataTable, int rowIndex) {
    return dataTable.item(rowIndex).getAttributes();
  }

  /**
   * @param dataTable the node list which has columns and rows
   * @return a sorted list of column names
   */
  protected List<String> getSortedColumnNames(NodeList dataTable) {
    List<String> columnNames = new ArrayList<>();
    NamedNodeMap firstRow = getRowFromTable(dataTable, 0);
    
    for (int i = 0; i < firstRow.getLength(); i++) {
      columnNames.add(firstRow.item(i).getNodeName());
    }
    
    Collections.sort(columnNames);
    return columnNames;
  }
  
  protected String getCellFromRow(NamedNodeMap row, String columnName) {
    Node node = row.getNamedItem(columnName);
    if (node != null) {
      return node.getNodeValue();
    } else {
      return DUMMY_DATA.toString();
    }
  }

  protected RowAsArray<Object> getRowAsArray(NodeList dataTable, int rowIndex) {
    List<String> columnNames = getSortedColumnNames(dataTable);
    Object[] row = new Object[columnNames.size()];
    int numberOfRows = dataTable.getLength();
    if (numberOfRows >= rowIndex) {
      NamedNodeMap rowDataFromTable = getRowFromTable(dataTable, rowIndex);
      int i = 0;
      for (String columnName : columnNames) {
        row[i] = getCellFromRow(rowDataFromTable, columnName);
        ++i;
      }
    }
    return new RowAsArray<>(row);
  }
}
