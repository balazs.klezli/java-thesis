package data.provider;

/**
 * Enumeration of the types of the data providers.
 */
public enum ModelType {
  LocalSingleWorkerXml,
  LocalSingleWorkerJson,
  InternetRowWorkerFaceBook,
  InternetRowWorkerOpenShift,
  InternetCellFootballWinner,
  InternetSingleWorkerXml,
  InternetSingleWorkerJson;

  @Override
  public String toString() {
    switch (this) {
      case LocalSingleWorkerXml:
        return "Local XML";
      case LocalSingleWorkerJson:
        return "Local Json";
      case InternetRowWorkerFaceBook:
        return "Facebook";
      case InternetRowWorkerOpenShift:
        return "OpenShift";
      case InternetCellFootballWinner:
        return "Football winner";
      case InternetSingleWorkerXml:
        return "Net XML";
      case InternetSingleWorkerJson:
        return "Net Json";
    }
    return null;
  }
}
