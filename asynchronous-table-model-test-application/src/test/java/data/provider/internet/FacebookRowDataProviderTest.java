package data.provider.internet;

import asyncronous.table.data.provider.RowAsList;
import data.provider.helper.Helper;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class FacebookRowDataProviderTest {
  private static final String URL_FACEBOOK_GRAPH_API_PREFIX = "https://graph.facebook.com/";
  private static final String[] COLUMN_NAMES = {"id", "name", "link", "about", "fan_count"};
  private static final String JSON_RELATIVE_PATH_0 = "/facebookProvider0.json";
  private static final String JSON_RELATIVE_PATH_1 = "/facebookProvider1.json";
  private static final String JSON_RELATIVE_PATH_2 = "/facebookProvider2.json";

  private final Helper helper = new Helper();

  @Captor
  private ArgumentCaptor<String> stringArgumentCaptor;

  @Mock
  private Helper helperMock;

  @InjectMocks
  private FacebookRowDataProvider underTest;

  @BeforeTest
  public void doBeforeTest() throws Exception {
    initMocks(this);
    underTest = new FacebookRowDataProvider(helperMock);
  }

  @Test
  public void shouldReturnRowWhenGetRowAsArrayCalledWithValidRowIndex() throws Exception {
    // Given
    String json0 = helper.getStringFromInputStream(getClass()
            .getResourceAsStream(JSON_RELATIVE_PATH_0));
    String json1 = helper.getStringFromInputStream(getClass()
            .getResourceAsStream(JSON_RELATIVE_PATH_1));
    String json2 = helper.getStringFromInputStream(getClass()
            .getResourceAsStream(JSON_RELATIVE_PATH_2));

    when(helperMock.jsonStringFromUrl(startsWith(URL_FACEBOOK_GRAPH_API_PREFIX))).thenReturn(json0, json1, json2);

    // When
    RowAsList<String> rowZero = underTest.getRow(0);
    RowAsList<String> rowOne = underTest.getRow(1);
    RowAsList<String> rowSecond = underTest.getRow(2);

    // Then
    assertEquals(rowZero.size(), 5);
    assertEquals(rowZero.get(0), "1563838817166050");
    assertEquals(rowZero.get(1), "Coca-Cola");
    assertEquals(rowZero.get(2), "https://www.facebook.com/cocacolahu/");
    assertEquals(rowZero.get(3), "The Coca-Cola Facebook Page is a collection of your stories showing how people" +
            " from around the world have helped make Coke into what it is today.");
    assertEquals(rowZero.get(4), "100553391");
    assertEquals(rowOne.size(), 5);
    assertEquals(rowOne.get(0), "148886831837137");
    assertEquals(rowOne.get(1), "McDonald's");
    assertEquals(rowOne.get(2), "https://www.facebook.com/mcdonaldshungary/");
    assertEquals(rowOne.get(4), "67924189");
    assertEquals(rowSecond.size(), 5);
    assertEquals(rowSecond.get(0), "111281682413");
    assertEquals(rowSecond.get(1), "Microsoft Lumia");
    assertEquals(rowSecond.get(2), "https://www.facebook.com/LumiaSegitseg/");
    assertEquals(rowSecond.get(4), "47039880");
  }

  @Test
  public void shouldReturnListOfColumnNamesWhenGetColumnNamesCalled() {
    List<String> actualColumnNames = underTest.getColumnNames();

    assertEquals(actualColumnNames.size(), COLUMN_NAMES.length);
    for (int i = 0; i < actualColumnNames.size(); ++i) {
      assertEquals(actualColumnNames.get(i), COLUMN_NAMES[i]);
    }
  }

  @Test
  public void shouldReturnNumberOfRowsWhenGetRowSizeCalled() {
    int expectedNumberOfRows = 40;

    int numberOfRows = underTest.getRowSize();

    assertEquals(numberOfRows, expectedNumberOfRows);
  }

  @Test
  public void shouldReturnNumberOfColumnWhenGetColumnSizeCalled() {
    int expectedNumberOfColumns = 5;

    int actualNumberOfColumns = underTest.getColumnSize();

    assertEquals(actualNumberOfColumns, expectedNumberOfColumns);
  }
}
