package data.provider.internet;

import asyncronous.table.data.provider.RowAsList;
import com.google.gson.JsonParser;
import data.provider.helper.Helper;
import org.mockito.Mock;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class OpenShiftRowDataProviderTest {
  private static final String URL_GET_COLUMN_NAMES = "http://thesis-bklezli.rhcloud.com/rest/members/columnNames";
  private static final String URL_GET_NUMBER_OF_MEMBERS = "http://thesis-bklezli.rhcloud.com/rest/members/number";
  private static final String URL_PART_GET_MEMBERS = "http://thesis-bklezli.rhcloud.com/rest/members/";

  private static final String COLUMN_NAMES = "[\"id\",\"name\",\"email\",\"phoneNumber\"]";
  private static final String[] ROW_DATA = {
          "{\"id\":0,\"name\":\"test user 0\",\"email\":\"testUser0@example.com\",\"phoneNumber\":\"19197543700\"}",
          "{\"id\":1,\"name\":\"test user 1\",\"email\":\"testUser1@example.com\",\"phoneNumber\":\"19197543701\"}",
          "{\"id\":2,\"name\":\"test user 2\",\"email\":\"testUser2@example.com\",\"phoneNumber\":\"19197543702\"}",
          "{\"id\":3,\"name\":\"test user 3\",\"email\":\"testUser3@example.com\",\"phoneNumber\":\"19197543703\"}",
          "{\"id\":4,\"name\":\"test user 4\",\"email\":\"testUser4@example.com\",\"phoneNumber\":\"19197543704\"}",
          "{\"id\":5,\"name\":\"test user 5\",\"email\":\"testUser5@example.com\",\"phoneNumber\":\"19197543705\"}",
          "{\"id\":6,\"name\":\"test user 6\",\"email\":\"testUser6@example.com\",\"phoneNumber\":\"19197543706\"}",
          "{\"id\":7,\"name\":\"test user 7\",\"email\":\"testUser7@example.com\",\"phoneNumber\":\"19197543707\"}",
          "{\"id\":8,\"name\":\"test user 8\",\"email\":\"testUser8@example.com\",\"phoneNumber\":\"19197543708\"}",
          "{\"id\":9,\"name\":\"test user 9\",\"email\":\"testUser9@example.com\",\"phoneNumber\":\"19197543709\"}",
          "{\"id\":10,\"name\":\"test user 10\",\"email\":\"testUser10@example.com\",\"phoneNumber\":\"19197543710\"}"
  };

  private static final int NUMBER_OF_ROWS = 11;

  @Mock
  private Helper helperMock;

  private OpenShiftRowDataProvider underTest;

  @BeforeTest
  public void doBeforeTest() throws Exception {
    initMocks(this);
    createDefaultMocking();
    underTest = new OpenShiftRowDataProvider(helperMock);
  }

  @Test
  public void shouldReturnRowWhenGetRowAsArrayCalledWithValidRowIndex() throws Exception {
    // Given
    String partOfMobileNumber = "\"191975437";
    String partOfUserName = "\"test user ";
    String emailPrefix = "\"testUser";
    String emailPostfix = "@example.com\"";

    List<String[]> expectedRows = new ArrayList<>();
    IntStream.range(0, NUMBER_OF_ROWS - 1).forEach(i -> {
      String[] row = {i + "", partOfUserName + i + "\"", emailPrefix + i + emailPostfix, partOfMobileNumber + "0" + i + "\""};
      expectedRows.add(row);
    });

    String[] row = {"10", partOfUserName + "10\"", emailPrefix + "10" + emailPostfix, partOfMobileNumber + "10\""};
    expectedRows.add(row);

    // When
    List<RowAsList<Object>> rows = new ArrayList<>();
    for (int i = 0; i < NUMBER_OF_ROWS; ++i) {
      rows.add(underTest.getRow(i));
    }

    // Then
    assertEquals(rows.size(), NUMBER_OF_ROWS);
    IntStream.range(0, NUMBER_OF_ROWS).forEach(i -> {
      assertEquals(rows.get(i).get(0).toString(), expectedRows.get(i)[0]);
      assertEquals(rows.get(i).get(1).toString(), expectedRows.get(i)[1]);
      assertEquals(rows.get(i).get(2).toString(), expectedRows.get(i)[2]);
      assertEquals(rows.get(i).get(3).toString(), expectedRows.get(i)[3]);
    });
  }

  @Test
  public void shouldReturnListOfColumnNamesWhenGetColumnNamesCalled() throws Exception {
    List<String> actualColumnNames = underTest.getColumnNames();

    assertEquals(actualColumnNames.size(), 4);
    assertEquals(actualColumnNames.get(0), "id");
    assertEquals(actualColumnNames.get(1), "name");
    assertEquals(actualColumnNames.get(2), "email");
    assertEquals(actualColumnNames.get(3), "phoneNumber");
  }

  @Test
  public void shouldReturnNumberOfRowsWhenGetRowSizeCalled() throws Exception {
    int numberOfRows = underTest.getRowSize();

    assertEquals(numberOfRows, NUMBER_OF_ROWS);
  }

  @Test
  public void shouldReturnNumberOfColumnWhenGetColumnSizeCalled() throws Exception {
    int expectedNumberOfColumns = 4;

    int actualNumberOfColumns = underTest.getColumnSize();

    assertEquals(actualNumberOfColumns, expectedNumberOfColumns);
  }

  private void createDefaultMocking() throws Exception {
    JsonParser parser = new JsonParser();
    when(helperMock.jsonElementFromUrl(eq(URL_GET_COLUMN_NAMES))).thenReturn(parser.parse(COLUMN_NAMES));
    when(helperMock.jsonElementFromUrl(eq(URL_GET_NUMBER_OF_MEMBERS))).thenReturn(parser.parse("11"));

    for (int i = 0; i < ROW_DATA.length; ++i) {
      when(helperMock.jsonElementFromUrl(eq(URL_PART_GET_MEMBERS + i))).thenReturn(parser.parse(ROW_DATA[i]));
    }
  }

}
