package data.provider.internet;

import data.provider.helper.Helper;
import org.mockito.Mock;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.IntStream;

import static asyncronous.table.data.provider.BaseDataProvider.DUMMY_DATA;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class FootballLeagueWinnerCellDataProviderTest {
  private static final String URL_PREFIX = "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=content&titles=";
  private static final String[] LEAGUES = {"YEARS", "DFB-Supercup | DFL-Supercup", "Supercoppa_Italiana", "Supercopa_de_España"};

  private static final String JSON_RELATIVE_PATH_0 = "/footballLeagueWinnerCellDataProvider_DFL-Supercup.json";
  private static final String JSON_RELATIVE_PATH_1 = "/footballLeagueWinnerCellDataProvider_Supercoppa_Italiana.json";
  private static final String JSON_RELATIVE_PATH_2 = "/footballLeagueWinnerCellDataProvider_Supercopa_de_Espana.json";

  private final Helper helper = new Helper();

  @Mock
  private Helper helperMock;

  private FootballLeagueWinnerCellDataProvider underTest;

  @BeforeTest
  public void doBeforeTest() throws Exception {
    initMocks(this);
    underTest = new FootballLeagueWinnerCellDataProvider(helperMock);
  }

  @Test
  public void shouldReturnCellDataWhenGetDataFromIndexCalledWithValidRowAndColumnIndex() throws Exception {
    // Given
    String json0 = helper.getStringFromInputStream(getClass()
            .getResourceAsStream(JSON_RELATIVE_PATH_0));
    String json1 = helper.getStringFromInputStream(getClass()
            .getResourceAsStream(JSON_RELATIVE_PATH_1));
    String json2 = helper.getStringFromInputStream(getClass()
            .getResourceAsStream(JSON_RELATIVE_PATH_2));

    when(helperMock.jsonStringFromUrl(startsWith(URL_PREFIX))).thenReturn(json0, json1, json2);

    // When
    Object cell_10_1 = underTest.getCellData(10, 1);
    Object cell_10_2 = underTest.getCellData(10, 2);
    Object cell_10_3 = underTest.getCellData(10, 3);

    // Then
    verify(helperMock, times(3)).jsonStringFromUrl(any());
    assertEquals(cell_10_1, "FC Bayern Munich");
    assertEquals(cell_10_2, "S.S.C. Napoli");
    assertEquals(cell_10_3, "Real Madrid C.F.");
  }

  @Test
  public void shouldReturnYearDataWhenGetDataFromIndexCalledWithValidRowAndColumnIndex() throws Exception {
    // Given
    int firstYear = 1980;

    // When
    Object result_0 = underTest.getCellData(0, 0);
    Object result_10 = underTest.getCellData(10, 0);

    // Then
    assertEquals(result_0, firstYear + "");
    assertEquals(result_10, (firstYear + 10) + "");
  }

  @Test
  public void shouldReturnWithDummyDataWhenGetDataFromIndexCalledWithIndexWithoutData() throws Exception {
    // Given
    when(helperMock.jsonStringFromUrl(any())).thenReturn("");

    // When
    Object result = underTest.getCellData(0, 1);

    // Then
    assertEquals(result, DUMMY_DATA);
  }

  @Test
  public void shouldReturnListOfColumnNamesWhenGetColumnNamesCalled() throws Exception {
    List<String> actualColumnNames = underTest.getColumnNames();

    assertEquals(actualColumnNames.size(), LEAGUES.length);
    IntStream
            .range(0, LEAGUES.length)
            .forEach(i -> assertEquals(actualColumnNames.get(i), LEAGUES[i]));
  }

  @Test
  public void shouldReturnNumberOfRowsWhenGetRowSizeCalled() throws Exception {
    int firstYear = 1980;
    int lastYear = new GregorianCalendar().get(Calendar.YEAR);

    int numberOfRows = underTest.getRowSize();

    assertEquals(numberOfRows, lastYear - firstYear + 1);
  }

  @Test
  public void shouldReturnNumberOfColumnWhenGetColumnSizeCalled() throws Exception {
    int expectedNumberOfColumns = 4;

    int actualNumberOfColumns = underTest.getColumnSize();

    assertEquals(actualNumberOfColumns, expectedNumberOfColumns);
  }
}
