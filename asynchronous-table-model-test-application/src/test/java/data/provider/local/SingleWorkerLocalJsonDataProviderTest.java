package data.provider.local;

import asyncronous.table.data.provider.RowAsArray;
import data.provider.helper.Helper;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class SingleWorkerLocalJsonDataProviderTest {
  private static final String[] COLUMN_NAMES = {"id", "first_name", "last_name", "email", "gender", "ip_address"};

  private SingleWorkerLocalJsonDataProvider underTest;

  @BeforeTest
  public void doBeforeTest() throws Exception {
    Helper helper = new Helper();
    underTest = new SingleWorkerLocalJsonDataProvider(helper);
  }

  @Test
  public void shouldReturnRowWhenGetRowAsArrayCalledWithValidRowIndex() throws Exception {
    // Given
    String[][] firstTenExpectedRow = {
            {"1", "Antonio", "Thompson", "athompson0@smh.com.au", "Male", "144.79.226.44"},
            {"2", "Kimberly", "Harvey", "kharvey1@wix.com", "Female", "151.162.14.127"},
            {"3", "Eric", "Cox", "ecox2@t.co", "Male", "241.56.216.151"},
            {"4", "Jesse", "Bowman", "jbowman3@usatoday.com", "Male", "36.204.81.138"},
            {"5", "Phillip", "Woods", "pwoods4@mac.com", "Male", "22.126.69.196"},
            {"6", "Christopher", "Ross", "cross5@usatoday.com", "Male", "88.164.243.222"},
            {"7", "John", "Morrison", "jmorrison6@rambler.ru", "Male", "224.119.52.139"},
            {"8", "Patricia", "Brown", "pbrown7@vistaprint.com", "Female", "235.252.15.238"},
            {"9", "Johnny", "West", "jwest8@ameblo.jp", "Male", "177.53.249.200"},
            {"10", "John", "Lewis", "jlewis9@sohu.com", "Male", "101.47.240.56"}};

    // When
    List<RowAsArray<String>> dataAsListOfRows = underTest.getListOfRows();

    // Then
    assertEquals(dataAsListOfRows.size(), 1000);
    for (int i = 0; i < dataAsListOfRows.size(); ++i) {
      assertEquals(dataAsListOfRows.get(i).valueCount(), COLUMN_NAMES.length);
    }

    for (int i = 0; i < 10; ++i) {
      for (int j = 0; j < COLUMN_NAMES.length; ++j) {
        assertEquals(dataAsListOfRows.get(i).valueAt(j), firstTenExpectedRow[i][j]);
      }
    }
  }

  @Test
  public void shouldReturnListOfColumnNamesWhenGetColumnNamesCalled() throws Exception {
    List<String> actualColumnNames = underTest.getColumnNames();

    assertEquals(actualColumnNames.size(), COLUMN_NAMES.length);
    for (int i = 0; i < actualColumnNames.size(); ++i) {
      assertEquals(actualColumnNames.get(i), COLUMN_NAMES[i]);
    }
  }

  @Test
  public void shouldReturnNumberOfRowsWhenGetRowSizeCalled() throws Exception {
    int expectedNumberOfRows = 1000;

    int numberOfRows = underTest.getRowSize();

    assertEquals(numberOfRows, expectedNumberOfRows);
  }

  @Test
  public void shouldReturnNumberOfColumnWhenGetColumnSizeCalled() throws Exception {
    int expectedNumberOfColumns = 6;

    int actualNumberOfColumns = underTest.getColumnSize();

    assertEquals(actualNumberOfColumns, expectedNumberOfColumns);
  }
}
