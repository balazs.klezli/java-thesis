package data.provider.local;

import asyncronous.table.data.provider.RowAsArray;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

import static asyncronous.table.data.provider.BaseDataProvider.DUMMY_DATA;
import static org.testng.Assert.assertEquals;

public class SingleWorkerLocalXmlDataProviderTest {
  private static final String[] COLUMN_NAMES = {
          "capital", "car_code", "datacode", "gdp_agri", "gdp_total", "government",
          "id", "indep_date", "infant_mortality", "inflation", "name", "population",
          "population_growth", "total_area"};

  private SingleWorkerLocalXmlDataProvider underTest;

  @BeforeTest
  public void doBeforeTest() throws Exception {
    underTest = new SingleWorkerLocalXmlDataProvider();
  }

  @Test
  public void shouldReturnRowWhenGetRowAsArrayCalledWithValidRowIndex() throws Exception {
    // Given
    // When
    List<RowAsArray<Object>> dataAsListOfRows = underTest.getListOfRows();

    // Then
    assertEquals(dataAsListOfRows.size(), 20);
    for (int i = 0; i < dataAsListOfRows.size(); ++i) {
      assertEquals(dataAsListOfRows.get(i).valueCount(), COLUMN_NAMES.length);
    }

    assertEquals(dataAsListOfRows.get(0).valueAt(10).toString(), "Hungary");
    assertEquals(dataAsListOfRows.get(0).valueAt(11).toString(), "10002541");
    assertEquals(dataAsListOfRows.get(0).valueAt(13).toString(), "93030");

    assertEquals(dataAsListOfRows.get(1).valueAt(10).toString(), "Holy See");
    assertEquals(dataAsListOfRows.get(1).valueAt(3).toString(), DUMMY_DATA);
    assertEquals(dataAsListOfRows.get(1).valueAt(4).toString(), DUMMY_DATA);

    assertEquals(dataAsListOfRows.get(5).valueAt(10).toString(), "Germany");
    assertEquals(dataAsListOfRows.get(5).valueAt(11).toString(), "83536112");
    assertEquals(dataAsListOfRows.get(5).valueAt(13).toString(), "356910");

    assertEquals(dataAsListOfRows.get(19).valueAt(10).toString(), "Albania");
    assertEquals(dataAsListOfRows.get(19).valueAt(11).toString(), "3249136");
    assertEquals(dataAsListOfRows.get(19).valueAt(13).toString(), "28750");
  }

  @Test
  public void shouldReturnListOfColumnNamesWhenGetColumnNamesCalled() throws Exception {
    List<String> actualColumnNames = underTest.getColumnNames();

    assertEquals(actualColumnNames.size(), COLUMN_NAMES.length);
    for (int i = 0; i < actualColumnNames.size(); ++i) {
      assertEquals(actualColumnNames.get(i), COLUMN_NAMES[i]);
    }
  }

  @Test
  public void shouldReturnNumberOfRowsWhenGetRowSizeCalled() throws Exception {
    int expectedNumberOfRows = 20;

    int numberOfRows = underTest.getRowSize();

    assertEquals(numberOfRows, expectedNumberOfRows);
  }

  @Test
  public void shouldReturnNumberOfColumnWhenGetColumnSizeCalled() throws Exception {
    int expectedNumberOfColumns = 14;

    int actualNumberOfColumns = underTest.getColumnSize();

    assertEquals(actualNumberOfColumns, expectedNumberOfColumns);
  }
}
