package asynchronous.list.data.provider;

/**
 * Data provider interface for list models which extends {@link BaseDataProvider} and has a function for getting a list element based on a given index.
 * @param <E> type of the list elements
 */
public interface ItemsDataProvider<E> extends BaseDataProvider<E> {
  /**
   * Returns elements from the given index
   *
   * @param index
   *          position of the element
   * @return E element
   * @throws java.lang.Exception when some error occurred during data retrieval
   */
  E getDataFromIndex(int index) throws Exception;
}
