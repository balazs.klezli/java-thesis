package asynchronous.list.data.provider;

/**
 * Data provider interface for list models which provides the size of the list
 * and the dummy element
 * 
 * @param <E> type of the list elements
 */

public interface BaseDataProvider<E> {

  String DEFAULT_LOADING_STRING = "loading...";

  /**
   * @return with the size of the data
   * @throws java.lang.Exception when some error occurred during row size retrieval
   */
  Integer getSize() throws Exception;

  /**
   * Returns dummy element while the background service try to get the real
   * data.
   * 
   * @return dummy data
   */
  E getDummyData();
}
