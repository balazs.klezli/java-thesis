package asynchronous.list.data.provider;

import java.util.List;

/**
 * Data provider interface for list models which extends {@link BaseDataProvider} and has a function for getting all list elements.
 * @param <E> type of list elements
 */
public interface ListedItemsDataProvider<E> extends BaseDataProvider<E> {
  /**
   * Returns all list elements in a {@link List}
   * @return Returns all list elements
   * @throws java.lang.Exception when some error occurred during data retrieval
   */
  List<E> getData() throws Exception;
}
