package asynchronous.list.model;

import asynchronous.exception.ExceptionHandler;
import asynchronous.exception.ExceptionHandlerImpl;
import asynchronous.exception.event.ExceptionListener;
import asynchronous.list.data.provider.BaseDataProvider;

import javax.swing.*;
import java.util.concurrent.ExecutionException;

import static javax.swing.SwingWorker.StateValue.PENDING;

/**
 * Abstract asynchronous implementation of {@link AbstractListModel}.
 * @param <E> the type of the elements of this model
 */
public abstract class AbstractAsyncListModel<E> extends AbstractListModel<E> {

  private final ExceptionHandler eh = new ExceptionHandlerImpl();

  protected SizeSwingWorker sizeWorker = new SizeSwingWorker();

  /**
   * data provider for the list model.
   */
  protected final BaseDataProvider<E> dataProvider;

  public AbstractAsyncListModel(BaseDataProvider<E> dataProvider) {
    this.dataProvider = dataProvider;
  }

  /**
   * Invalidates elementAt SwingWorkers.
   */
  protected abstract void invalidateElementAtWorkers();

  /**
   * Function to add an exception listener which will be informed when some error occurres in the data provider.
   * @param listener the listener which will be added to the exception listeners
   */
  public void addExceptionListener(ExceptionListener listener) {
    eh.addExceptionListener(listener);
  }

  /**
   * Function to remove an exception listener.
   * @param listener the listener which will be removed from the exception listeners
   */
  public void removeExceptionListener(ExceptionListener listener) {
    eh.removeExceptionListener(listener);
  }

  /**
   * Notifies the listeners about the given exception.
   * @param exception the exception which will be sent to the listeners
   */
  protected void fireExceptionEvent(Exception exception) {
    eh.fireExceptionEvent(exception);
  }

  private void refreshSizeWorker() {
    sizeWorker = new SizeSwingWorker();
    sizeWorker.execute();
  }

  /**
   * Invalidates all data retrieved from the provider. This will cause the {@link JList}s to be refreshed.
   */
  public void invalidateData() {
    refreshSizeWorker();
    invalidateElementAtWorkers();
    fireContentsChanged(this, 0, getSize());
  }

  @Override
  public int getSize() {
    int size = 0;

    if (sizeWorker.isDone()) {
      size = sizeWorker.getSize();
    } else if (sizeWorker.isInitialState()) {
      sizeWorker.execute();
    }

    return size;
  }

  /**
   * {@link SwingWorker} implementation which retrieves size information based on the data provider.
   */
  protected class SizeSwingWorker extends SwingWorker<Integer, Void> {
    private int size;

    public int getSize() {
      return size;
    }

    public boolean isInitialState() {
      return this.getState() == PENDING;
    }

    @Override
    protected Integer doInBackground() throws Exception {
      return dataProvider.getSize();
    }

    @Override
    protected void done() {
      try {
        size = get();
      } catch (InterruptedException e) {
        fireExceptionEvent(e);
      } catch (ExecutionException e) {
        fireExceptionEvent((Exception) e.getCause());
      }
      fireIntervalAdded(this, 0, size);
    }
  }
}
