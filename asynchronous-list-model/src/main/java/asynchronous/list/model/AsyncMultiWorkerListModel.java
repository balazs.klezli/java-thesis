package asynchronous.list.model;

import asynchronous.list.data.provider.ItemsDataProvider;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static javax.swing.SwingWorker.StateValue.PENDING;

/**
 * Asynchronous implementation of {@link AbstractAsyncListModel}. It can be used as an asynchronous model for a {@link JList}.
 * The constructor method requires an implementation of the {@link ItemsDataProvider} as a parameter.
 *
 * @param <E> the type of the elements of this model
 */
public class AsyncMultiWorkerListModel<E> extends AbstractAsyncListModel<E> {

  private final ItemsDataProvider<E> indexedItemsDataProvider;

  private final List<ElementAtSwingWorker> listOfElementAtSwingWorkers = new ArrayList<>();

  private final HashMap<Integer, Boolean> invalidDataInThread = new HashMap<>();

  public AsyncMultiWorkerListModel(ItemsDataProvider<E> indexedItemsDataProvider) {
    super(indexedItemsDataProvider);
    this.indexedItemsDataProvider = indexedItemsDataProvider;
  }

  private boolean shouldRemakeWorkerThread(int index) {
    return invalidDataInThread.get(index) == null ? false : invalidDataInThread.get(index);
  }

  private ElementAtSwingWorker getSwingWorkerFromList(int index) {
    if (listOfElementAtSwingWorkers.size() - 1 >= index) {
      return listOfElementAtSwingWorkers.get(index);
    }

    return null;
  }

  @Override
  public void invalidateElementAtWorkers() {
    for (Integer key : invalidDataInThread.keySet()) {
      invalidDataInThread.put(key, true);
    }
  }

  @Override
  public E getElementAt(int index) {
    ElementAtSwingWorker elementAtWorker = getSwingWorkerFromList(index);
    boolean shouldReplaceAnOldWorker = shouldRemakeWorkerThread(index);

    if (elementAtWorker != null && shouldReplaceAnOldWorker) {
      listOfElementAtSwingWorkers.remove(elementAtWorker);
    }

    if (shouldReplaceAnOldWorker || elementAtWorker == null) {
      elementAtWorker = new ElementAtSwingWorker(index);
      listOfElementAtSwingWorkers.add(index, elementAtWorker);
      elementAtWorker.execute();
      invalidDataInThread.put(index, false);
    }

    return elementAtWorker.getElement();
  }
  /**
   * {@link SwingWorker} implementation which retrieves a list item based on the given index.
   * It uses the {@link ItemsDataProvider}
   */
  class ElementAtSwingWorker extends SwingWorker<E, Void> {
    private final int index;

    private E element = null;

    public ElementAtSwingWorker(int index) {
      this.index = index;
    }

    public boolean isInitialState() {
      return this.getState() == PENDING;
    }

    @Override
    protected E doInBackground() throws Exception {
      return indexedItemsDataProvider.getDataFromIndex(index);
    }

    @Override
    protected void done() {
      try {
        element = get();
      } catch (InterruptedException e) {
        fireExceptionEvent(e);
      } catch (ExecutionException e) {
        fireExceptionEvent((Exception) e.getCause());
      }
      fireContentsChanged(this, index, index);
    }

    public E getElement() {
      if (element == null) {
        return dataProvider.getDummyData();
      }
      return element;
    }
  }
}
