package asynchronous.list.model;

import asynchronous.list.data.provider.ListedItemsDataProvider;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static javax.swing.SwingWorker.StateValue.PENDING;


/**
 * Asynchronous implementation of {@link AbstractAsyncListModel}. It can be used as an asynchronous model for a {@link JList}.
 * The constructor method requires an implementation of the {@link ListedItemsDataProvider} as a parameter.
 *
 * @param <E> the type of the elements of this model
 */
public class AsyncSingleWorkerListModel<E> extends AbstractAsyncListModel<E> {

  private ElementsSwingWorker elementsSwingWorker = new ElementsSwingWorker();

  private final ListedItemsDataProvider<E> listedItemsDataProvider;

  private boolean validData = true;

  public AsyncSingleWorkerListModel(ListedItemsDataProvider<E> listedItemsDataProvider) {
    super(listedItemsDataProvider);
    this.listedItemsDataProvider = listedItemsDataProvider;
  }

  @Override
  public E getElementAt(int index) {
    if (elementsSwingWorker.isInitialState()) {
      elementsSwingWorker.execute();
    } else if (shouldRefreshData()) {
      elementsSwingWorker = new ElementsSwingWorker();
      elementsSwingWorker.execute();
      validData = true;
    } else if (elementsSwingWorker.isDone()) {
      E element = null;
      try {
        if (!elementsSwingWorker.getElements().isEmpty()) {
          element = elementsSwingWorker.getElements().get(index);
        }
      } catch (Exception e) {
        fireExceptionEvent(e);
      }
      return element;
    }
    return dataProvider.getDummyData();
  }

  private boolean shouldRefreshData() {
    return !validData;
  }

  @Override
  public void invalidateElementAtWorkers() {
    validData = false;
  }

  /**
   * {@link SwingWorker} implementation which retrieves the list of elements.
   * It uses the {@link ListedItemsDataProvider}
   */
  class ElementsSwingWorker extends SwingWorker<List<E>, Void> {

    private List<E> elements = new ArrayList<>();

    public List<E> getElements() {
      return elements;
    }

    public boolean isInitialState() {
      return this.getState() == PENDING;
    }

    @Override
    protected List<E> doInBackground() throws Exception {
      return listedItemsDataProvider.getData();
    }

    @Override
    protected void done() {
      try {
        elements = get();
      } catch (InterruptedException | ExecutionException e) {
        fireExceptionEvent(e);
      }
      fireContentsChanged(this, 0, elements.size());
    }
  }
}
