package asynchronous.list.model;

import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;
import asynchronous.list.data.provider.ItemsDataProvider;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.swing.event.ListDataListener;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;

public class AsyncMultiWorkerListModelTest {

  private static final String ANY_LIST_ELEMENT = "anyListElement";

  private static final String ANY_PROVIDER_EXCEPTION_MESSAGE = "anyProviderErrorHappened";

  private static final int OFFSET_OF_THE_LIST_ELEMENTS = 5;

  private static final Object SYNC_OBJECT = new Object();

  private ArgumentCaptor<ExceptionEvent> exceptionEventArgumentCaptor;

  @Mock
  private ItemsDataProvider<String> dataProviderMock;

  @Mock
  private ExceptionListener exceptionListenerMock;

  @Mock
  private ListDataListener listDataListenerMock;

  AsyncMultiWorkerListModel<String> underTest;

  @BeforeMethod
  public void init() {
    MockitoAnnotations.initMocks(this);
    exceptionEventArgumentCaptor = ArgumentCaptor.forClass(ExceptionEvent.class);

    underTest = new AsyncMultiWorkerListModel<>(dataProviderMock);
    underTest.addExceptionListener(exceptionListenerMock);
    underTest.addListDataListener(listDataListenerMock);
  }

  @Test
  public void shouldReturnWithDummyDataWhenGetElementAtCalledWithoutWaiting() throws Exception {
    // When
    underTest.getElementAt(0);

    // Then
    verify(dataProviderMock, atLeastOnce()).getDummyData();
  }

  @Test
  public void shouldReturnElementAtIndexWhenGetElementAtCalledAndWaitForBackgroundWorker() throws Exception {
    // Given
    int elementIndex = 3;
    List<String> listElements = createListElements(10);
    when(dataProviderMock.getDataFromIndex(elementIndex))
            .thenReturn(listElements.get(elementIndex));

    // When
    underTest.getElementAt(0);
    underTest.getElementAt(1);
    underTest.getElementAt(2);
    underTest.getElementAt(elementIndex);
    verify(listDataListenerMock, after(600).times(4)).contentsChanged(any());
    String thirdElement = underTest.getElementAt(elementIndex);

    // Then
    assertEquals(thirdElement, ANY_LIST_ELEMENT + elementIndex);
  }

  @Test
  public void shouldFireExceptionEventWhenGetChildElementAtCalledAndSomethingWentWrongInTheBackendThread() throws Exception {
    // Given
    when(dataProviderMock.getDataFromIndex(0)).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.getElementAt(0);

    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(600);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }

  @Test
  public void shouldReloadDataIfInvalidateElementAtWorkersCalled() throws Exception {
    // Given
    int elementIndex = 3;
    List<String> listElements = createListElements(10);
    List<String> otherListElements = createAnotherListElements(10);
    when(dataProviderMock.getDataFromIndex(elementIndex))
            .thenReturn(listElements.get(elementIndex))
            .thenReturn(otherListElements.get(elementIndex));

    // When
    underTest.getElementAt(0);
    underTest.getElementAt(1);
    underTest.getElementAt(2);
    underTest.getElementAt(elementIndex);
    verify(listDataListenerMock, after(600).times(4)).contentsChanged(any());
    String thirdElement = underTest.getElementAt(elementIndex);
    assertEquals(thirdElement, ANY_LIST_ELEMENT + elementIndex);

    underTest.invalidateElementAtWorkers();
    underTest.getElementAt(0);
    underTest.getElementAt(1);
    underTest.getElementAt(2);
    underTest.getElementAt(elementIndex);
    verify(listDataListenerMock, after(1200).times(8)).contentsChanged(any());
    thirdElement = underTest.getElementAt(elementIndex);

    // Then
    assertEquals(thirdElement, ANY_LIST_ELEMENT + (elementIndex + OFFSET_OF_THE_LIST_ELEMENTS));
  }

  private List<String> createListElements(final int numberOfListElements) {
    List<String> listElements = new ArrayList<>();
    IntStream.range(0, numberOfListElements).forEach(i -> listElements.add(ANY_LIST_ELEMENT + i));
    return listElements;
  }

  private List<String> createAnotherListElements(final int numberOfListElements) {
    List<String> listElements = new ArrayList<>();
    IntStream.range(OFFSET_OF_THE_LIST_ELEMENTS, OFFSET_OF_THE_LIST_ELEMENTS + numberOfListElements).forEach(i -> listElements.add(ANY_LIST_ELEMENT + i));
    return listElements;
  }
}
