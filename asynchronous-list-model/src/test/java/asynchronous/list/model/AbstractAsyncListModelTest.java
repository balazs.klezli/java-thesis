package asynchronous.list.model;

import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;
import asynchronous.list.data.provider.ListedItemsDataProvider;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.swing.event.ListDataListener;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.after;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.assertEquals;

/**
 * Test class for testing {@link AbstractAsynchronousListModel} but because this is an abstract class
 * {@link AsyncSingleWorkerListModel} is used.
 */
public class AbstractAsyncListModelTest {

  private static final String ANY_LIST_ELEMENT = "anyListElement";

  private static final String ANY_PROVIDER_EXCEPTION_MESSAGE = "anyProviderErrorHappened";

  private static final Object SYNC_OBJECT = new Object();

  private ArgumentCaptor<ExceptionEvent> exceptionEventArgumentCaptor;

  @Mock
  private ListedItemsDataProvider<String> dataProviderMock;

  @Mock
  private ExceptionListener exceptionListenerMock;

  @Mock
  private ListDataListener listDataListenerMock;

  AsyncSingleWorkerListModel<String> underTest;

  @BeforeMethod
  public void init() {
    MockitoAnnotations.initMocks(this);
    exceptionEventArgumentCaptor = ArgumentCaptor.forClass(ExceptionEvent.class);

    underTest = new AsyncSingleWorkerListModel<>(dataProviderMock);
    underTest.addExceptionListener(exceptionListenerMock);
    underTest.addListDataListener(listDataListenerMock);
  }

  @Test
  public void shouldReturnWithZeroWhenGetSizeWithoutWaiting() throws Exception {
    // When
    int size = underTest.getSize();

    // Then
    assertEquals(size, 0);
  }

  @Test
  public void shouldReturnSizeOfTheListWhenGetSizeCalledAndWaitForBackgroundWorker() throws Exception {
    // Given
    List<String> listElements = createListElements(10);
    when(dataProviderMock.getSize()).thenReturn(listElements.size());

    // When
    underTest.getSize();
    verify(listDataListenerMock, after(200).atLeastOnce()).intervalAdded(any());
    int size = underTest.getSize();

    // Then
    assertEquals(size, listElements.size());
  }

  @Test
  public void shouldFireExceptionEventWhenGetChildElementAtCalledAndSomethingWentWrongInTheBackendThread() throws Exception {
    // Given
    when(dataProviderMock.getSize()).thenThrow(new Exception(ANY_PROVIDER_EXCEPTION_MESSAGE));

    // When
    underTest.getSize();

    synchronized (SYNC_OBJECT) {
      SYNC_OBJECT.wait(200);
    }

    // Then
    verify(exceptionListenerMock).exceptionReceived(exceptionEventArgumentCaptor.capture());
    String exceptionMessage = exceptionEventArgumentCaptor.getValue().getException().getMessage();
    assertThat(exceptionMessage, equalTo(ANY_PROVIDER_EXCEPTION_MESSAGE));
  }

  @Test
  public void shouldReloadDataIfInvalidateCalled() throws Exception {
    // Given
    List<String> listElements = createListElements(13);
    List<String> otherListElements = createAnotherListElements(21);
    when(dataProviderMock.getSize())
            .thenReturn(listElements.size())
            .thenReturn(otherListElements.size());

    // When
    underTest.getSize();
    verify(listDataListenerMock, after(200).atLeastOnce()).intervalAdded(any());
    int size = underTest.getSize();
    assertEquals(size, listElements.size());

    underTest.invalidateData();
    verify(listDataListenerMock, after(200).times(2)).intervalAdded(any());
    size = underTest.getSize();

    // Then
    assertEquals(size, otherListElements.size());
  }

  private List<String> createListElements(final int numberOfListElements) {
    List<String> listElements = new ArrayList<>();
    IntStream.range(0, numberOfListElements).forEach(i -> listElements.add(ANY_LIST_ELEMENT + i));
    return listElements;
  }

  private List<String> createAnotherListElements(final int numberOfListElements) {
    int offset = 5;
    List<String> listElements = new ArrayList<>();
    IntStream.range(offset, offset + numberOfListElements).forEach(i -> listElements.add(ANY_LIST_ELEMENT + i));
    return listElements;
  }
}
