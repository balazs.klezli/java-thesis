package data.provider.internet;

import asynchronous.tree.data.provider.ChildrenCountDataProvider;
import data.provider.helper.Helper;

import javax.swing.tree.DefaultMutableTreeNode;

public class OpenShiftProviderForTwiceNumberOfChildrenWorkers extends OpenShiftProviderForNumberOfChildrenWorkers implements ChildrenCountDataProvider<DefaultMutableTreeNode> {

  private static final String GET_COUNT_OF_CHILDREN_URL_PART = "http://thesis-bklezli.rhcloud.com/rest/treenodes/countOfChildren/";

  public OpenShiftProviderForTwiceNumberOfChildrenWorkers(Helper helper) {
    super(helper);
  }

  @Override
  public Integer getChildrenCount(DefaultMutableTreeNode parent) throws Exception {
    return helper.jsonElementFromUrl(GET_COUNT_OF_CHILDREN_URL_PART + getParentId(parent)).getAsInt();
  }
}
