package data.provider.internet;

import asynchronous.tree.data.provider.ChildrenDataProvider;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import data.provider.helper.Helper;
import data.provider.userobject.OpenShiftTreeNode;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.ArrayList;
import java.util.List;

public class OpenShiftProviderForNumberOfChildrenWorkers implements ChildrenDataProvider<DefaultMutableTreeNode> {
  protected Helper helper;

  protected static final String GET_ROOT_NODE_URL = "http://thesis-bklezli.rhcloud.com/rest/treenodes/root";
  protected static final String GET_CHILDREN_URL_PART = "http://thesis-bklezli.rhcloud.com/rest/treenodes/children/";

  protected static final OpenShiftTreeNode.NodeBuilder NODE_BUILDER = new OpenShiftTreeNode.NodeBuilder();

  protected static final String JSON_ATTR_ID = "id";
  protected static final String JSON_ATTR_PARENT_ID = "parentId";
  protected static final String JSON_ATTR_NAME = "name";

  public OpenShiftProviderForNumberOfChildrenWorkers(Helper helper) {
    this.helper = helper;
  }

  @Override
  public DefaultMutableTreeNode getRoot() throws Exception {
    JsonObject rootElement = helper.jsonElementFromUrl(GET_ROOT_NODE_URL).getAsJsonObject();

    Long rootId = rootElement.get(JSON_ATTR_ID).getAsLong();
    String rootName = rootElement.get(JSON_ATTR_NAME).getAsString();

    OpenShiftTreeNode rootOSTN = NODE_BUILDER.setId(rootId).setName(rootName).setParentId(null).build();

    return new DefaultMutableTreeNode(rootOSTN);
  }

  @Override
  public List<DefaultMutableTreeNode> getChildren(DefaultMutableTreeNode parent) throws Exception {
    List<DefaultMutableTreeNode> children = new ArrayList<>();
    JsonArray childrenArray = helper.jsonElementFromUrl(GET_CHILDREN_URL_PART + getParentId(parent)).getAsJsonArray();

    for (int i = 0; i < childrenArray.size(); ++i) {
      JsonObject child = childrenArray.get(i).getAsJsonObject();
      Long childId = child.get(JSON_ATTR_ID).getAsLong();
      Long childParentId = child.get(JSON_ATTR_PARENT_ID).getAsLong();
      String childName = child.get(JSON_ATTR_NAME).getAsString();

      OpenShiftTreeNode childOSTN = NODE_BUILDER.setId(childId).setName(childName).setParentId(childParentId).build();
      final DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(childOSTN);
      treeNode.setParent(parent);
      children.add(treeNode);
    }

    return children;
  }

  protected Long getParentId(DefaultMutableTreeNode node) {
    return ((OpenShiftTreeNode) node.getUserObject()).getId();
  }
}
