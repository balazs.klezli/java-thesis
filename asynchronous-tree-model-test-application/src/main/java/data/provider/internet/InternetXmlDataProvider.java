package data.provider.internet;

import asynchronous.tree.data.provider.StructureDataProvider;
import data.provider.helper.Helper;
import org.w3c.dom.Document;

import javax.swing.tree.TreeNode;

/**
 * TreeDataProvider implementation which uses the Internet for getting data.
 */
public class InternetXmlDataProvider implements StructureDataProvider<TreeNode> {

  private static final String URL_STRING = "http://ropman.web.elte.hu/thesis/lemonade.xml";

  private final Helper helper = new Helper();

  @Override
  public TreeNode getData() throws Exception {
    Document doc = helper.getDocumentFromUrl(URL_STRING);
    return helper.makeTreeModel(doc);
  }

}
