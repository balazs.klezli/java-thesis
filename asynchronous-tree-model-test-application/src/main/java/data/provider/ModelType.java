package data.provider;

/**
 * Enumeration of the types of the data providers.
 */
public enum ModelType {
  LocalXmlWithOneWorker,
  InternetXmlWithOneWorker,
  OpenShiftProviderWithChildrenCountWorker,
  OpenShiftProviderWithoutChildrenCountWorker;

  @Override
  public String toString() {
    switch (this) {
      case LocalXmlWithOneWorker:
        return "Local XML";
      case InternetXmlWithOneWorker:
        return "Net XML";
      case OpenShiftProviderWithChildrenCountWorker:
        return "OpenShift counted";
      case OpenShiftProviderWithoutChildrenCountWorker:
        return "OpenShift non-counted";
    }
    return null;
  }
}
