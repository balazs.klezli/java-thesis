package data.provider.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Helper {

  public String getStringFromInputStream(InputStream is) throws IOException {

    BufferedReader br = null;
    StringBuilder sb = new StringBuilder();

    String line;
    try {

      br = new BufferedReader(new InputStreamReader(is));
      while ((line = br.readLine()) != null) {
        sb.append(line);
      }

    } finally {
      if (br != null) {
        br.close();
      }
    }

    return sb.toString();
  }

  public InputStream inputStreamFromUrl(String urlString) throws IOException {
    URL url = new URL(urlString);
    URLConnection connection = url.openConnection();
    return connection.getInputStream();
  }

  public Document getDocumentFromUrl(String urlString) throws Exception {
    InputStream inputStream = inputStreamFromUrl(urlString);
    return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
  }

  public String jsonStringFromUrl(String url) throws IOException {
    return getStringFromInputStream(inputStreamFromUrl(url));
  }

  public JsonElement jsonElementFromUrl(String url) throws Exception {
    String json = jsonStringFromUrl(url);
    return new JsonParser().parse(json);
  }

  public TreeNode makeTreeModel(Document doc) {
    Node rootFromDoc = doc.getChildNodes().item(0);
    String rootName = rootFromDoc.getNodeName();
    DefaultMutableTreeNode treeRoot = new DefaultMutableTreeNode(rootName);
    addChildrenTo(rootFromDoc, treeRoot);
    return treeRoot;
  }

  private void addChildrenTo(Node node, DefaultMutableTreeNode parentTreeNode) {
    NodeList nl = node.getChildNodes();

    int countOfChildren = nl.getLength();
    for (int i = 0; i < countOfChildren; ++i) {
      Node child = nl.item(i);
      DefaultMutableTreeNode childTreeNode = new DefaultMutableTreeNode(child.getNodeName());
      parentTreeNode.add(childTreeNode);
      addChildrenTo(child, childTreeNode);
    }
  }
}
