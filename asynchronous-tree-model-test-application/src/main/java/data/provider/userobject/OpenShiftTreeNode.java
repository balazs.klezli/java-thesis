package data.provider.userobject;

public class OpenShiftTreeNode {
  private final Long id;
  private final Long parentId;
  private final String name;

  private OpenShiftTreeNode(Long id, Long parentId, String name) {
    this.id = id;
    this.parentId = parentId;
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public Long getparentId() {
    return parentId;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return name;
  }

  public static class NodeBuilder {
    private Long id;
    private Long parentId;
    private String name;

    public NodeBuilder() {
    }

    public NodeBuilder setId(Long id) {
      this.id = id;
      return this;
    }

    public NodeBuilder setName(String name) {
      this.name = name;
      return this;
    }

    public NodeBuilder setParentId(Long parentId) {
      this.parentId = parentId;
      return this;
    }

    public OpenShiftTreeNode build() {
      return new OpenShiftTreeNode(id, parentId, name);
    }
  }

}
