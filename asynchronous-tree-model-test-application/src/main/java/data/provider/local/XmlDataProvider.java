package data.provider.local;

import asynchronous.tree.data.provider.StructureDataProvider;
import data.provider.helper.Helper;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.swing.tree.TreeNode;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;

public class XmlDataProvider implements StructureDataProvider<TreeNode> {

  private static final String RELATIVE_PATH = "/lemonade.xml";

  private static final int MAX_SLEEP_TIME_IN_MILLISEC = 1000;

  private final SecureRandom random = new SecureRandom();

  private TreeNode root;

  private final Helper helper = new Helper();

  public XmlDataProvider() throws IOException, SAXException, ParserConfigurationException {
    this(null);
  }

  public XmlDataProvider(String path) throws ParserConfigurationException, IOException, SAXException {
    InputStream inputStream;
    if (path == null) {
      inputStream = getClass().getResourceAsStream(RELATIVE_PATH);
    } else {
      inputStream = getClass().getResourceAsStream(path);
    }
    Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
    root = helper.makeTreeModel(doc);
  }

  @Override
  public TreeNode getData() throws Exception {
    sleepRandomTime(MAX_SLEEP_TIME_IN_MILLISEC);
    return root;
  }

  private void sleepRandomTime(int maxSleepTime) throws InterruptedException {
    int sleepTime = random.nextInt(maxSleepTime);
    Thread.sleep(sleepTime);
  }
}
