import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;
import asynchronous.tree.model.AbstractAsyncTreeModel;
import asynchronous.tree.model.AsyncStructureWorkerTreeModel;
import asynchronous.tree.model.AsyncChildrenCountWorkerTreeModel;
import asynchronous.tree.model.AsyncChildrenWorkerTreeModel;
import data.provider.ModelType;
import data.provider.helper.Helper;
import data.provider.internet.InternetXmlDataProvider;
import data.provider.internet.OpenShiftProviderForNumberOfChildrenWorkers;
import data.provider.internet.OpenShiftProviderForTwiceNumberOfChildrenWorkers;
import data.provider.local.XmlDataProvider;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.SOUTH;

/**
 * Main class for testing asynchronous tree model implementations
 */
public class TreeModelTest extends JFrame implements ActionListener {

  private static final String INVALIDATE_BTN_TEXT = "invalidate data";
  private static final String ACTION_INVALIDATE = "invalidate";
  private static final Helper HELPER = new Helper();

  private final JTree tree;
  private JTextArea exceptionsArea;
  private AbstractAsyncTreeModel model;
  private JButton invalidateBtn;

  public static void main(String[] args) {
    EventQueue.invokeLater(()->new TreeModelTest().setVisible(true));
  }

  public TreeModelTest() {
    setLayout(new BorderLayout());
    setSize(600, 400);
    setTitle("Tree Model Example");
    setLocationRelativeTo(null);
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    tree = new JTree(new DefaultMutableTreeNode("Choose a model type from the toolbox, and click"));
    getContentPane().add(new JScrollPane(tree), BorderLayout.CENTER);
    fillBottomContainer(getBottomContainer());

    final JToolBar toolBar = new JToolBar();
    getContentPane().add(toolBar, BorderLayout.NORTH);
    for(ModelType modelType : ModelType.values()) {
      toolBar.add(new NewModelAction(modelType));
    }
  }

  private class NewModelAction extends AbstractAction {
    private final ModelType modelType;

    public NewModelAction(ModelType modelType) {
      super(modelType.toString());
      this.modelType = modelType;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      switch (modelType) {
        case InternetXmlWithOneWorker:
          model = new AsyncStructureWorkerTreeModel(new InternetXmlDataProvider());
          break;
        case LocalXmlWithOneWorker:
          try {
            model = new AsyncStructureWorkerTreeModel(new XmlDataProvider());
          } catch (IOException | SAXException | ParserConfigurationException ex) {
            exceptionsArea.append(ex.getMessage());
            exceptionsArea.append("\n");
            ex.printStackTrace(System.err);
            return;
          }
          break;
        case OpenShiftProviderWithoutChildrenCountWorker:
          model = new AsyncChildrenWorkerTreeModel(new OpenShiftProviderForNumberOfChildrenWorkers(HELPER));
          break;
        case OpenShiftProviderWithChildrenCountWorker:
          model = new AsyncChildrenCountWorkerTreeModel(new OpenShiftProviderForTwiceNumberOfChildrenWorkers(HELPER));
          break;
        default: throw new IllegalStateException("Not all modeltypes are covered in switch");
      }
      model.addExceptionListener(new ModelExceptionListener());
      invalidateBtn.setEnabled(true);
      tree.setModel(model);
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (ACTION_INVALIDATE.equals(e.getActionCommand())) {
      model.invalidateData();
    }
  }

  private void fillBottomContainer(Container bottomContainer) {
    invalidateBtn = new JButton(INVALIDATE_BTN_TEXT);
    invalidateBtn.addActionListener(this);
    invalidateBtn.setActionCommand(ACTION_INVALIDATE);
    invalidateBtn.setEnabled(false);
    bottomContainer.add(invalidateBtn, CENTER);

    exceptionsArea = new JTextArea("List of occurred Exceptions:\n\n");
    JScrollPane scroll = new JScrollPane(exceptionsArea);
    scroll.setPreferredSize(new Dimension(scroll.getWidth(), 100));
    bottomContainer.add(scroll, SOUTH);
  }

  private Container getBottomContainer() {
    Container c = new Container();
    c.setLayout(new BorderLayout());
    add(c, SOUTH);
    return c;
  }

  private class ModelExceptionListener implements ExceptionListener {

    @Override
    public void exceptionReceived(ExceptionEvent event) {
      final Exception exception = event.getException();
      SwingUtilities.invokeLater(() -> {
        exceptionsArea.append("**** Beginning of exception description\n");
        exceptionsArea.append("Class of the exception: " + exception.getClass() + "\n");
        exceptionsArea.append("Message of the exception: " + exception.getMessage() + "\n");
        exceptionsArea.append("**** End of exception description\n\n");
        exception.printStackTrace(System.err);
      });
    }
  }
}
