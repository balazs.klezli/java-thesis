package data.provider.internet;

import org.mockito.InjectMocks;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class SingleWorkerInternetXmlDataProviderTest {

  private static final String RELATIVE_PATH = "src/test/resources/lemonade.xml";

  private static final String NODE_NAME_FOR_0 = "inventory";
  private static final String NODE_NAME_FOR_0_0 = "drink";
  private static final String NODE_NAME_FOR_0_0_0 = "lemonade";
  private static final String NODE_NAME_FOR_0_0_0_0 = "price";
  private static final String NODE_NAME_FOR_0_0_0_1 = "amount";
  private static final String NODE_NAME_FOR_0_0_1 = "pop";
  private static final String NODE_NAME_FOR_0_0_1_0 = "price";
  private static final String NODE_NAME_FOR_0_0_1_1 = "amount";
  private static final String NODE_NAME_FOR_0_1 = "snack";
  private static final String NODE_NAME_FOR_0_1_0 = "chips";
  private static final String NODE_NAME_FOR_0_1_0_0 = "price";
  private static final String NODE_NAME_FOR_0_1_0_1 = "amount";

  private DefaultMutableTreeNode rootNode_0;
  private DefaultMutableTreeNode node_0_0;
  private DefaultMutableTreeNode node_0_0_0;
  private DefaultMutableTreeNode node_0_0_0_0;
  private DefaultMutableTreeNode node_0_0_1;
  private DefaultMutableTreeNode node_0_0_1_0;
  private DefaultMutableTreeNode node_0_1;
  private DefaultMutableTreeNode node_0_1_0;

  private Document doc = null;

  private FileInputStream lemonadeInputStream;

  @InjectMocks
  private InternetXmlDataProvider underTest;


  @BeforeTest
  public void doBeforeTest() throws SAXException, IOException, ParserConfigurationException {
    initMocks(this);

    underTest = new InternetXmlDataProvider();
    lemonadeInputStream = new FileInputStream(RELATIVE_PATH);
    doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(lemonadeInputStream);
    createTreeNodes();
  }

  @Test
  public void shouldReturnWithFullTreeDataWhenGetDataCalled() throws Exception {
    TreeNode rootNode = underTest.getData();

    assertEquals(rootNode.toString(), NODE_NAME_FOR_0);
    assertEquals(rootNode.getChildCount(), 2);
    assertEquals(rootNode.getChildAt(0).toString(), NODE_NAME_FOR_0_0);
    assertEquals(rootNode.getChildAt(1).toString(), NODE_NAME_FOR_0_1);
    assertEquals(rootNode.getChildAt(1).getChildCount(), 1);
    assertEquals(rootNode.getChildAt(1).getChildAt(0).toString(), NODE_NAME_FOR_0_1_0);
    assertEquals(rootNode.getChildAt(1).getChildAt(0).getChildCount(), 2);
    assertEquals(rootNode.getChildAt(1).getChildAt(0).getChildAt(0).toString(), NODE_NAME_FOR_0_1_0_0);
    assertEquals(rootNode.getChildAt(1).getChildAt(0).getChildAt(1).toString(), NODE_NAME_FOR_0_1_0_1);
    assertEquals(rootNode.getChildAt(0).getChildCount(), 2);
    assertEquals(rootNode.getChildAt(0).getChildAt(0).toString(), NODE_NAME_FOR_0_0_0);
    assertEquals(rootNode.getChildAt(0).getChildAt(1).toString(), NODE_NAME_FOR_0_0_1);
    assertEquals(rootNode.getChildAt(0).getChildAt(1).getChildCount(), 2);
    assertEquals(rootNode.getChildAt(0).getChildAt(1).getChildAt(0).toString(), NODE_NAME_FOR_0_0_1_0);
    assertEquals(rootNode.getChildAt(0).getChildAt(1).getChildAt(1).toString(), NODE_NAME_FOR_0_0_1_1);
    assertEquals(rootNode.getChildAt(0).getChildAt(0).getChildCount(), 2);
    assertEquals(rootNode.getChildAt(0).getChildAt(0).getChildAt(0).toString(), NODE_NAME_FOR_0_0_0_0);
    assertEquals(rootNode.getChildAt(0).getChildAt(0).getChildAt(1).toString(), NODE_NAME_FOR_0_0_0_1);
  }

  private void createTreeNodes() {
    rootNode_0 = new DefaultMutableTreeNode(NODE_NAME_FOR_0);
    node_0_0 = new DefaultMutableTreeNode(NODE_NAME_FOR_0_0);
    rootNode_0.add(node_0_0);
    node_0_0_0 = new DefaultMutableTreeNode(NODE_NAME_FOR_0_0_0);
    node_0_0.add(node_0_0_0);
    node_0_0_0_0 = new DefaultMutableTreeNode(NODE_NAME_FOR_0_0_0_0);
    node_0_0_0.add(node_0_0_0_0);
    node_0_0_1 = new DefaultMutableTreeNode(NODE_NAME_FOR_0_0_1);
    node_0_0.add(node_0_0_1);
    node_0_0_1_0 = new DefaultMutableTreeNode(NODE_NAME_FOR_0_0_1_0);
    node_0_0_1.add(node_0_0_1_0);
    node_0_1 = new DefaultMutableTreeNode(NODE_NAME_FOR_0_1);
    rootNode_0.add(node_0_1);
    node_0_1_0 = new DefaultMutableTreeNode(NODE_NAME_FOR_0_1_0);
    node_0_1.add(node_0_1_0);


    node_0_0_0.add(new DefaultMutableTreeNode(NODE_NAME_FOR_0_0_0_1));
    node_0_0_1.add(new DefaultMutableTreeNode(NODE_NAME_FOR_0_0_1_1));
    node_0_1_0.add(new DefaultMutableTreeNode(NODE_NAME_FOR_0_1_0_0));
    node_0_1_0.add(new DefaultMutableTreeNode(NODE_NAME_FOR_0_1_0_1));
  }
}
