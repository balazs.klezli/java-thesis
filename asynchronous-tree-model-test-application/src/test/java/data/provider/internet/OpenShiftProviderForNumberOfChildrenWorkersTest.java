package data.provider.internet;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import data.provider.helper.Helper;
import data.provider.userobject.OpenShiftTreeNode;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

import static data.provider.internet.OpenShiftProviderForNumberOfChildrenWorkers.NODE_BUILDER;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class OpenShiftProviderForNumberOfChildrenWorkersTest {

  protected static final String GET_ROOT_NODE_URL = "http://thesis-bklezli.rhcloud.com/rest/treenodes/root";

  protected static final String GET_CHILDREN_URL_PART = "http://thesis-bklezli.rhcloud.com/rest/treenodes/children/";

  @Mock
  private Helper helperMock;

  @InjectMocks
  private OpenShiftProviderForNumberOfChildrenWorkers underTest;

  @BeforeTest
  public void doBeforeTest() throws SAXException, IOException, ParserConfigurationException {
    initMocks(this);
    underTest = new OpenShiftProviderForNumberOfChildrenWorkers(helperMock);
  }

  @Test
  public void shouldReturnRootNodeWhenGetRootCalled() throws Exception {
    createMockingForRootNode();

    DefaultMutableTreeNode result = underTest.getRoot();

    assertNotNull(result, "RootNode must be not null");
    assertEquals(((OpenShiftTreeNode) result.getUserObject()).getId(), new Long(0));
    assertEquals(((OpenShiftTreeNode) result.getUserObject()).getName(), "rootElement");
  }

  @Test
  public void shouldReturnChildrenWhenGetChildrenCalledWithParentNode() throws Exception {
    DefaultMutableTreeNode parentNode = createParentNodeWithGetChildrenMocking();

    List<DefaultMutableTreeNode> children = underTest.getChildren(parentNode);

    assertEquals(children.size(), 2);
    assertEquals(children.get(0).toString(), "child1");
    assertEquals(children.get(1).toString(), "child2");

  }

  private void createMockingForRootNode() throws Exception {
    JsonObject rootElement = new JsonObject();
    rootElement.addProperty("id", 0);
    rootElement.addProperty("name", "rootElement");

    when(helperMock.jsonElementFromUrl(GET_ROOT_NODE_URL)).thenReturn(rootElement);
  }

  private DefaultMutableTreeNode createParentNodeWithGetChildrenMocking() throws Exception {
    JsonObject child1 = new JsonObject();
    child1.addProperty("id", 1);
    child1.addProperty("parentId", "0");
    child1.addProperty("name", "child1");

    JsonObject child2 = new JsonObject();
    child2.addProperty("id", 2);
    child2.addProperty("parentId", "0");
    child2.addProperty("name", "child2");

    JsonArray children = new JsonArray();
    children.add(child1);
    children.add(child2);
    when(helperMock.jsonElementFromUrl(GET_CHILDREN_URL_PART + "0")).thenReturn(children);

    OpenShiftTreeNode rootOSTN = NODE_BUILDER.setId(0L).setName("rootElement").setParentId(null).build();
    return new DefaultMutableTreeNode(rootOSTN);
  }
}
