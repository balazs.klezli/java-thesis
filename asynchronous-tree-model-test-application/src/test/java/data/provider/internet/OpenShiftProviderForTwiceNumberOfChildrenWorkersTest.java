package data.provider.internet;

import com.google.gson.*;
import data.provider.helper.Helper;
import data.provider.userobject.OpenShiftTreeNode;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

import static data.provider.internet.OpenShiftProviderForNumberOfChildrenWorkers.NODE_BUILDER;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class OpenShiftProviderForTwiceNumberOfChildrenWorkersTest {
  private static final String GET_COUNT_OF_CHILDREN_URL_PART = "http://thesis-bklezli.rhcloud.com/rest/treenodes/countOfChildren/";

  @Mock
  private Helper helperMock;

  @InjectMocks
  private OpenShiftProviderForTwiceNumberOfChildrenWorkers underTest;

  @BeforeTest
  public void doBeforeTest() throws SAXException, IOException, ParserConfigurationException {
    initMocks(this);
    underTest = new OpenShiftProviderForTwiceNumberOfChildrenWorkers(helperMock);
  }

  @Test
  public void shouldReturnChildrenCountWhenGetChildrenCountCalled() throws Exception {
    // Given
    JsonElement numberOfChildren = new JsonPrimitive(2);
    when(helperMock.jsonElementFromUrl(Matchers.eq(GET_COUNT_OF_CHILDREN_URL_PART + "1")))
            .thenReturn(numberOfChildren);

    OpenShiftTreeNode parentOSTN = NODE_BUILDER.setId(1L).setName("parentElement").setParentId(0L).build();
    DefaultMutableTreeNode parent = new DefaultMutableTreeNode(parentOSTN);

    // When
    int result = underTest.getChildrenCount(parent);

    // Then
    assertEquals(result, 2, "should return the number of the children");
  }
}
