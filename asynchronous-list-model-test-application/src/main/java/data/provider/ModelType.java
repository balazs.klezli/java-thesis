package data.provider;

/**
 * Enumeration of the types of the data providers.
 */
public enum ModelType {
  InternetSingleWorker,
  InternetNumberOfItemsWorker,
  LocalSingleWorker,
  LocalNumberOfItemsWorker;

  @Override
  public String toString() {
    switch (this) {
      case LocalSingleWorker:
        return "Local Single";
      case InternetSingleWorker:
        return "Net Single";
      case LocalNumberOfItemsWorker:
        return "Local Multiple";
      case InternetNumberOfItemsWorker:
        return "Net Multiple";
    }
    return null;
  }
}
