package data.provider.local;

import asynchronous.list.data.provider.BaseDataProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * BaseDataProvider implementation which gets data from a local list. The provided
 * data are String objects.
 */
public abstract class AbstractLocalDataProvider implements BaseDataProvider<String> {

  protected List<String> getDummyList() throws InterruptedException {
    // simulating heavy calculation
    Thread.sleep(500);
    return generateDummyListWithElement(30);
  }

  private List<String> generateDummyListWithElement(int numberOfData) {
    List<String> dummyArray = new ArrayList<>();
    for (int i = 1; i <= numberOfData; ++i) {
      dummyArray.add(getDummyString(i));
    }
    return dummyArray;
  }

  private String getDummyString(int i) {
    switch (i) {
      case 1:
        return i + "st";
      case 2:
        return i + "nd";
      case 3:
        return i + "rd";
      default:
        return i + "th";
    }
  }

  @Override
  public Integer getSize() throws InterruptedException {
    return getDummyList().size();
  }

  @Override
  public String getDummyData() {
    return DEFAULT_LOADING_STRING;
  }
}
