package data.provider.local;

import asynchronous.list.data.provider.ListedItemsDataProvider;

import java.util.List;

/**
 * ListedItemsDataProvider implementation which gets data from a local list. The provided
 * data are String objects.
 */
public class LocalSingleWorkerDataProvider extends AbstractLocalDataProvider implements ListedItemsDataProvider<String> {

  @Override
  public List<String> getData() throws InterruptedException {
    return getDummyList();
  }

}
