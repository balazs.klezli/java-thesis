package data.provider.local;

import asynchronous.list.data.provider.ItemsDataProvider;

/**
 * ItemsDataProvider implementation which gets data from a local list. The provided
 * data are String objects.
 */
public class LocalNumberOfItemsWorkerDataProvider extends  AbstractLocalDataProvider implements ItemsDataProvider<String> {

  @Override
  public String getDataFromIndex(int index) throws InterruptedException {
    return getDummyList().get(index);
  }
}
