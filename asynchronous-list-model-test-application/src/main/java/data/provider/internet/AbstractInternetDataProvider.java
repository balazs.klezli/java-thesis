package data.provider.internet;

import asynchronous.list.data.provider.BaseDataProvider;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * BaseDataProvider implementation which gets data from the Internet. The provided data
 * are String objects.
 */
public abstract class AbstractInternetDataProvider implements BaseDataProvider<String> {
  private final static String URL_STRING = "http://www.w3schools.com/xml/cd_catalog.xml";

  protected NodeList getDataFromInternet() throws IOException, SAXException, ParserConfigurationException, InterruptedException {
    Thread.sleep(500);
    URL url = new URL(URL_STRING);
    URLConnection connection = url.openConnection();
    InputStream inputStream = connection.getInputStream();
    Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
    return doc.getElementsByTagName("TITLE");
  }

  @Override
  public Integer getSize() throws IOException, SAXException, ParserConfigurationException, InterruptedException {
    return getDataFromInternet().getLength();
  }

  @Override
  public String getDummyData() {
    return DEFAULT_LOADING_STRING;
  }
}
