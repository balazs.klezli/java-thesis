package data.provider.internet;

import asynchronous.list.data.provider.ItemsDataProvider;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * ItemsDataProvider implementation which gets data from the Internet. The provided data
 * are String objects.
 */
public class InternetNumberOfItemsWorkerDataProvider extends AbstractInternetDataProvider implements ItemsDataProvider<String> {

  @Override
  public String getDataFromIndex(int index) throws IOException, SAXException, ParserConfigurationException, InterruptedException {
    return getDataFromInternet().item(index).getTextContent();
  }
}
