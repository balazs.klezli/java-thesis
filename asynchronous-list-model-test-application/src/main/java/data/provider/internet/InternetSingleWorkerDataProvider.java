package data.provider.internet;

import asynchronous.list.data.provider.ListedItemsDataProvider;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ListedItemsDataProvider implementation which gets data from the Internet. The provided data
 * are String objects.
 */
public class InternetSingleWorkerDataProvider extends AbstractInternetDataProvider implements ListedItemsDataProvider<String> {
  @Override
  public List<String> getData() throws IOException, SAXException, ParserConfigurationException, InterruptedException {
    List<String> elements = new ArrayList<>();
    NodeList dataList = getDataFromInternet();
    for (int i = 0; i < dataList.getLength(); ++i) {
      elements.add(dataList.item(i).getTextContent());
    }
    return elements;
  }
}
