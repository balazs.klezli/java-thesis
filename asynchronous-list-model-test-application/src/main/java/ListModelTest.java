import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;
import asynchronous.list.model.AbstractAsyncListModel;
import asynchronous.list.model.AsyncMultiWorkerListModel;
import asynchronous.list.model.AsyncSingleWorkerListModel;
import data.provider.ModelType;
import data.provider.internet.InternetNumberOfItemsWorkerDataProvider;
import data.provider.internet.InternetSingleWorkerDataProvider;
import data.provider.local.LocalNumberOfItemsWorkerDataProvider;
import data.provider.local.LocalSingleWorkerDataProvider;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.SOUTH;
import static java.awt.EventQueue.invokeLater;

/**
 * Main class for testing AsynchronousListModel implementations
 */
public class ListModelTest extends JFrame implements ActionListener {

  private static final String INVALIDATE_BTN_TEXT = "invalidate data";
  private static final String ACTION_INVALIDATE = "invalidate";

  private AbstractAsyncListModel<String> model;
  private final JList<String> list;
  private JTextArea exceptionsArea;
  private JButton invalidateBtn;

  public static void main(String s[]) {
    invokeLater(() -> new ListModelTest().setVisible(true));
  }

  private ListModelTest() {
    setLayout(new BorderLayout());
    setSize(665, 400);
    setLocationRelativeTo(null);
    setTitle("List Model Example");
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    list = new JList<>(new DefaultListModel<>());
    getContentPane().add(new JScrollPane(list), BorderLayout.CENTER);
    fillBottomContainer(getBottomContainer());

    final JToolBar toolBar = new JToolBar();
    getContentPane().add(toolBar, BorderLayout.NORTH);
    for(ModelType modelType : ModelType.values()) {
      toolBar.add(new NewModelAction(modelType));
    }
  }

  private class NewModelAction extends AbstractAction {
    private final ModelType modelType;

    public NewModelAction(ModelType modelType) {
      super(modelType.toString());
      this.modelType = modelType;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      switch (modelType) {
        case LocalSingleWorker:
          model = new AsyncSingleWorkerListModel<>(new LocalSingleWorkerDataProvider());
          break;
        case LocalNumberOfItemsWorker:
          model = new AsyncMultiWorkerListModel<>(new LocalNumberOfItemsWorkerDataProvider());
          break;
        case InternetSingleWorker:
          model = new AsyncSingleWorkerListModel<>(new InternetSingleWorkerDataProvider());
          break;
        case InternetNumberOfItemsWorker:
          model = new AsyncMultiWorkerListModel<>(new InternetNumberOfItemsWorkerDataProvider());
          break;
      }
      model.addExceptionListener(new ListModelExceptionListener());
      invalidateBtn.setEnabled(true);
      list.setModel(model);
    }
  }

  private void fillBottomContainer(Container bottomContainer) {
    invalidateBtn = new JButton(INVALIDATE_BTN_TEXT);
    invalidateBtn.addActionListener(this);
    invalidateBtn.setActionCommand(ACTION_INVALIDATE);
    invalidateBtn.setEnabled(false);
    bottomContainer.add(invalidateBtn, CENTER);

    exceptionsArea = new JTextArea("List of occured exceptions:\n\n");
    JScrollPane scroll = new JScrollPane(exceptionsArea);
    scroll.setPreferredSize(new Dimension(scroll.getWidth(), 100));
    bottomContainer.add(scroll, SOUTH);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (ACTION_INVALIDATE.equals(e.getActionCommand())) {
      model.invalidateData();
    }
  }

  private Container getBottomContainer() {
    Container c = new Container();
    c.setLayout(new BorderLayout());
    add(c, SOUTH);
    return c;
  }

  private class ListModelExceptionListener implements ExceptionListener {

    @Override
    public void exceptionReceived(ExceptionEvent event) {
      final Exception exception = event.getException();
      SwingUtilities.invokeLater(() -> {
        exceptionsArea.append("**** Beginning of exception description\n");
        exceptionsArea.append("Class of the exception: " + exception.getClass() + "\n");
        exceptionsArea.append("Message of the exception: " + exception.getMessage() + "\n");
        exceptionsArea.append("**** End of exception description\n\n");
        exception.printStackTrace(System.err);
      });
    }
  }
}
