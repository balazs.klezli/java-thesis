package data.provider.local;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static asynchronous.list.data.provider.BaseDataProvider.DEFAULT_LOADING_STRING;
import static org.testng.Assert.assertEquals;

public class LocalNumberOfItemsWorkerDataProviderTest {

  private LocalNumberOfItemsWorkerDataProvider underTest;

  @BeforeTest
  public void doBeforeTest() throws Exception {
    underTest = new LocalNumberOfItemsWorkerDataProvider();
  }


  @Test
  public void shouldReturnWithDummyDataWhenGetDummyDataCalled() throws Exception {
    String result = underTest.getDummyData();

    assertEquals(result, DEFAULT_LOADING_STRING);
  }

  @Test
  public void shouldReturnNumberOfItemsWhenGetSizeCalled() throws Exception {
    int numberOfItems = underTest.getSize();

    assertEquals(numberOfItems, 30);
  }

  @Test
  public void shouldReturnWithListItemsWhenGetDataCalled() throws Exception {
    int[] indices = {0, 5, 9, 14, 25};
    String[] expectedItems = {"1st", "6th", "10th", "15th", "26th"};
    for (int i = 0; i < indices.length; ++i) {
      assertEquals(underTest.getDataFromIndex(indices[i]), expectedItems[i]);
    }
  }
}
