package data.provider.local;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

import static asynchronous.list.data.provider.BaseDataProvider.DEFAULT_LOADING_STRING;
import static org.testng.Assert.assertEquals;

public class LocalSingleWorkerDataProviderTest {

  private LocalSingleWorkerDataProvider underTest;

  @BeforeTest
  public void doBeforeTest() throws Exception {
    underTest = new LocalSingleWorkerDataProvider();
  }


  @Test
  public void shouldReturnWithDummyDataWhenGetDummyDataCalled() throws Exception {
    String result = underTest.getDummyData();

    assertEquals(result, DEFAULT_LOADING_STRING);
  }

  @Test
  public void shouldReturnNumberOfItemsWhenGetSizeCalled() throws Exception {
    int numberOfItems = underTest.getSize();

    assertEquals(numberOfItems, 30);
  }

  @Test
  public void shouldReturnWithListItemsWhenGetDataCalled() throws Exception {
    List<String> items = underTest.getData();

    assertEquals(items.size(), 30);
    assertEquals(items.get(0), "1st");
    assertEquals(items.get(5), "6th");
    assertEquals(items.get(9), "10th");
    assertEquals(items.get(14), "15th");
    assertEquals(items.get(25), "26th");
  }
}
