package data.provider.internet;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.net.URLConnection;
import java.util.List;

import static asynchronous.list.data.provider.BaseDataProvider.DEFAULT_LOADING_STRING;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class InternetSingleWorkerDataProviderTest {

  @Mock
  private URLConnection urlConnectionMock;

  @InjectMocks
  private InternetSingleWorkerDataProvider underTest;

  @BeforeTest
  public void doBeforeTest() throws Exception {
    initMocks(this);
    underTest = new InternetSingleWorkerDataProvider();
    InputStream inputStream = getClass().getResourceAsStream("/cd_catalog.xml");
    Mockito.when(urlConnectionMock.getInputStream()).thenReturn(inputStream);
  }

  @Test
  public void shouldReturnWithDummyDataWhenGetDummyDataCalled() throws Exception {
    String result = underTest.getDummyData();

    assertEquals(result, DEFAULT_LOADING_STRING);
  }

  @Test
  public void shouldReturnNumberOfItemsWhenGetSizeCalled() throws Exception {
    int numberOfItems = underTest.getSize();

    assertEquals(numberOfItems, 26);
  }

  @Test
  public void shouldReturnWithListItemsWhenGetDataCalled() throws Exception {
    List<String> items = underTest.getData();

    assertEquals(items.size(), 26);
    assertEquals(items.get(0), "Empire Burlesque");
    assertEquals(items.get(5), "One night only");
    assertEquals(items.get(9), "When a man loves a woman");
    assertEquals(items.get(14), "Tupelo Honey");
    assertEquals(items.get(25), "Unchain my heart");
  }
}
