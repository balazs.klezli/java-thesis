package data.provider.internet;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.net.URLConnection;

import static asynchronous.list.data.provider.BaseDataProvider.DEFAULT_LOADING_STRING;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class InternetNumberOfItemsWorkerDataProviderTest {
  @Mock
  private URLConnection urlConnectionMock;

  @InjectMocks
  private InternetNumberOfItemsWorkerDataProvider underTest;

  @BeforeTest
  public void doBeforeTest() throws Exception {
    initMocks(this);
    underTest = new InternetNumberOfItemsWorkerDataProvider();
    InputStream inputStream = getClass().getResourceAsStream("/cd_catalog.xml");
    Mockito.when(urlConnectionMock.getInputStream()).thenReturn(inputStream);
  }

  @Test
  public void shouldReturnWithDummyDataWhenGetDummyDataCalled() throws Exception {
    String result = underTest.getDummyData();

    assertEquals(result, DEFAULT_LOADING_STRING);
  }

  @Test
  public void shouldReturnNumberOfItemsWhenGetSizeCalled() throws Exception {
    int numberOfItems = underTest.getSize();

    assertEquals(numberOfItems, 26);
  }

  @Test
  public void shouldReturnWithListItemsWhenGetDataCalled() throws Exception {
    int[] indices = {0, 5, 9, 14, 25};
    String[] expectedItems = {"Empire Burlesque", "One night only", "When a man loves a woman", "Tupelo Honey", "Unchain my heart"};
    for (int i = 0; i < indices.length; ++i) {
      assertEquals(underTest.getDataFromIndex(indices[i]), expectedItems[i]);
    }
  }
}
