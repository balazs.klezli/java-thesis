package asynchronous.exception;

import asynchronous.exception.event.ExceptionListener;

/**
 * ExceptionHandler interface.
 */
public interface ExceptionHandler {

  /**
   * Function to add an exception listener which will be informed when some error occurres in a data provider.
   * @param listener the listener which will added to the exception listeners
   */
  void addExceptionListener(ExceptionListener listener);

  /**
   * Function to remove an exception listener
   * @param listener the listener which will removed from the exception listeners
   */
  void removeExceptionListener(ExceptionListener listener);

  /**
   * Notifies the listeners about the given exception
   * @param exception the exception which will be sent to the listeners
   */
  void fireExceptionEvent(Exception exception);
}
