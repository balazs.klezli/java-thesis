package asynchronous.exception.event;

import java.util.EventListener;

/**
 * Exception listener interface which is an {@link EventListener}.
 */
public interface ExceptionListener extends EventListener {

  /**
   * The function which will be called when the listener has to be notified upon the exception event.
   * @param event the event that contains the exception
   */
  void exceptionReceived(ExceptionEvent event);
}
