package asynchronous.exception.event;

import java.util.EventObject;

/**
 * Event object which is wrapping an {@link Exception} object.
 */
public class ExceptionEvent extends EventObject {

  private final Exception exception;
  
  public ExceptionEvent(Object source, Exception exception) {
    super(source);
    this.exception = exception;
  }

  /**
   * Getter method of the exception object.
   * @return the wrapped exception object
   */
  public Exception getException() {
    return exception;
  }
}
