package asynchronous.exception;

import java.util.ArrayList;
import java.util.List;

import asynchronous.exception.event.ExceptionEvent;
import asynchronous.exception.event.ExceptionListener;

/**
 * Implementation of {@link ExceptionHandler}
 */
public class ExceptionHandlerImpl implements ExceptionHandler {

  private final List<ExceptionListener> exceptionListeners = new ArrayList<>();

  @Override
  public void addExceptionListener(ExceptionListener listener) {
    exceptionListeners.add(listener);
  }

  @Override
  public void removeExceptionListener(ExceptionListener listener) {
    exceptionListeners.remove(listener);
  }

  @Override
  public void fireExceptionEvent(Exception exception) {
    ExceptionEvent exceptionEvent = new ExceptionEvent(this, exception);
    for (ExceptionListener listener : exceptionListeners) {
      listener.exceptionReceived(exceptionEvent);
    }
  }
}